# Makefile for AudioRouter

CC = gcc
STD = -std=gnu89
WARNINGS = -Wall -Wextra
DEFINES = -D _DEFAULT_SOURCE
DEFINES = -D _GNU_SOURCE

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__

OPTIMIZE = -O3 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
MARCH = -march=native
MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
CC_FLAGS += $(MARCH)
CC_FLAGS += $(MTUNE)

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable


FILES = AudioRouter.o 

FILES_D = AudioRouter_d.o



all: libaudiorouter.so 

debug: libaudiorouter_d.so 

CServer: CServer.o 

CServer_debug: CServer_d.o

CLIent: CLIent.o 
CLIent_debug: CLIent_d.o

dir: 
	mkdir -p bin

clean: dir
	rm -rf bin

install:
	cp bin/libaudiorouter.so /usr/lib/

install_debug:
	cp bin/libaudiorouter_d.so /usr/lib/






libaudiorouter.so: dir $(FILES) 
	$(CC) -shared -fPIC -s -o bin/libaudiorouter.so bin/*.o -pthread -lpodnet -lpulse -lpulse-simple $(CC_FLAGS)
	rm bin/*.o


libaudiorouter_d.so: dir $(FILES_D) 
	$(CC) -g -shared -fPIC -o bin/libaudiorouter_d.so bin/*_d.o -pthread -lpodnet_d -lpulse -lpulse-simple $(STD)
	rm bin/*.o





AudioRouter.o: dir
	$(CC) -fPIC -c -o bin/AudioRouter.o CAudioRouter/CAudioRouter.c $(CC_FLAGS)

AudioRouter_d.o: dir
	$(CC) -g -fPIC -c -o bin/AudioRouter_d.o CAudioRouter/CAudioRouter.c $(CC_FLAGS_D)




CServer.o: dir
	$(CC) -o bin/CServer CServer/Main.c -laudiorouter -lpodnet $(CC_FLAGS)

CServer_d.o: dir
	$(CC) -g -o bin/CServer CServer/Main.c -laudiorouter_d -lpodnet_d $(CC_FLAGS_D)







Test_Local: dir
	$(CC) -g -fPIC -o bin/Test_Local Test/Test_Local.c -laudiorouter_d -lpodnet_d $(CC_FLAGS_D)

Test_Multicast: dir
	$(CC) -g -fPIC -o bin/Test_Multicast Test/Test_Multicast.c -laudiorouter_d -lpodnet_d $(CC_FLAGS_D)




CLIent.o: dir
	$(CC) -o bin/CLIent Clients/CLIent/CLIent.c -lpodnet $(CC_FLAGS)

CLIent_d.o: dir
	$(CC) -g -o bin/CLIent_d Clients/CLIent/CLIent.c -lpodnet_d $(CC_FLAGS_D)