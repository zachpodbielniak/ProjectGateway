---
title: Check List
author: Zach Podbielniak
geometry: margin=0.75in
output: pdf_document
fontsize: 12pt
---

\tableofcontents

\break

# Overview

* Virtual Sinker
* Multicast Support
* MPD Integration
* GUI


\break

## Virtual Sinker

This will be a daemon for Pulse Audio that runs, and will take all inputted sound, and output it to all sinks that we specify. Essentially multicasting a single audio source.

* Get Pulse Audio Support
* Audio Daemon
* Dynamic One-To-Many Relationship (Vector?)
* Use Sockets For Inter-Process Communication
* Server Architecture
* IPv4 and IPv6 Support
* Multicast To Forward (See Multicast Section)
* Multicast To Recieve
* Built-In Network Relaying Support
* By Default Reads From Config File
	* This Enables Automated Relay Information


\break

## Multicast Support (Virtual Sinker)

This will be utilized in the Virtual Sinker to forward audio to the any relay stations

* IPv4 Multicast Support
	* Server Address - 239.255.20.22
	* Relay Address - 239.255.20.23
	* Allows 239.255.20.22/31 To Be Routed
	* Support To Change These
* IPv6 Multicast Support
	* Server Address - TBA
	* Relay Address - TBA
	* Support To Change Addresses


\break

## MPD Integration

Enable integration with MPD. Basically any client we support should have a way to view current song of MPD, and add songs, del songs, etc.

* Browse
* Add From USB
* Play Out To Virtual Sink


\break

## GUI

This will essentially be the client for our Virtual Sinker, communicating over TCP to the sinker.

* Basic GUI To Enable Or Disable Audio Output
* Basic GUI To Play / Pause And Stop
* Basic GUI To Show Status
* TCP Socket To Connect To Virtual Sinker
* Client Architecture


\break

## Ncurses Interface

This will be a text-based client for our Virtual Sinker, communicating over TCP to the sinker.

* Basic Output Selection
* Play / Pause, Stop
* Show Status
* TCP Socket To Connect To Sinker
* Client Architecture
