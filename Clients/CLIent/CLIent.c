#include <PodNet/PodNet.h>
#include <PodNet/Ascii.h>
#include <PodNet/CString/CString.h>
#include <PodNet/CContainers/CContainers.h>
#include <PodNet/CNetworking/CIpv4.h>
#include <PodNet/CNetworking/CIpv6.h>
#include <PodNet/CNetworking/CSocket4.h>
#include <PodNet/CNetworking/CSocket6.h>
#include <PodNet/CNetworking/CHttpsRequest.h>
#include <PodNet/CSystem/CSystem.h>
#include <PodNet/CProcess/CProcess.h>
#include <PodNet/CFile/CFile.h>




typedef struct __PARAMS
{
	CSTRING		csOutputBuffer[4096];
	CSTRING		csInputBuffer[4096];

	IPV4_ADDRESS	ip4Host;
	USHORT 		usPort;
	CSTRING 	csSecret[256];

	HANDLE 		hLock;

	HANDLE 		hFileIn;
	HANDLE		hFileOut;

} PARAMS, *LPPARAMS;




INTERNAL_OPERATION
VOID
__PrintMenu(
	_In_ 		LPPARAMS 	lpParams
){
	CSTRING csBuffer[4096];

	ZeroMemory(csBuffer, sizeof(csBuffer));

	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"\n" \
		"Project Gateway CLIent\n" \
		"----------------------\n" \
		"\n" \
		"h | help -- Print Help Menu\n" \
		"v | version -- Print Version\n" \
		"c | clear -- Clear Terminal\n" \
		"q | quit -- Quit\n" \
		"\n" \
		"p | pause -- Pause Audio Routing\n" \
		"r | resume -- Resume Audio Router\n" \
		"s | status -- Get Status\n" \
		"i | ingress -- Get Ingress Devices\n" \
		"e | egress -- Get Egress Devices\n" \
		"im | ingress-multicast -- Get Ingress Multicast Devices\n" \
		"em | egress-multicast -- Get Egress Multicast Devices\n" \
		"sinks -- List All Connectable Sinks\n" \
		"sources -- List All Connectable Sources\n" \
		"\n" \
		"h | host -- Set Host IPv4 Address\n" \
		"P | port -- Set Port\n" \
		"S | secret -- Set The Secret\n" \
		"\n"
	);

	WriteFile(lpParams->hFileOut, csBuffer, 0);
}



INTERNAL_OPERATION
VOID
__Clear(
	_In_ 		LPPARAMS	lpParams
){
	if (GetStandardOut() == lpParams->hFileOut)
	{ 
		system("clear"); 
		__PrintMenu(lpParams);
	}
}




INTERNAL_OPERATION
VOID
__LaunchRequest(
	_In_ 		LPPARAMS 	lpParams,
	_In_Z_ 		LPSTR 		lpszUrl
){
	HANDLE hRequest;
	LPSTR lpszResponse, lpszMessage;
	CSTRING csHost[4096];
	CSTRING csApiCall[4096];
	UARCHLONG ualBytes;

	ZeroMemory(csHost, sizeof(csHost));
	ZeroMemory(csApiCall, sizeof(csApiCall));

	lpszResponse = LocalAllocAndZero(65536);
	ConvertIpv4AddressToString(lpParams->ip4Host, csHost, sizeof(csHost) - 1);
	StringPrintFormatSafe(
		csApiCall,
		sizeof(csApiCall) - 1,
		"%s?Secret=%s",
		lpszUrl,
		lpParams->csSecret
	);

	hRequest = CreateHttpsRequest(
		HTTP_REQUEST_GET,
		HTTP_VERSION_1_1,
		csHost,
		lpParams->usPort,
		csApiCall,
		HTTP_CONNECTION_CLOSE,
		NULLPTR,
		HTTP_ACCEPT_PLAIN_TEXT,
		NULLPTR
	);

	ExecuteHttpsRequest(
		hRequest,
		lpszResponse,
		65535,
		&ualBytes
	);

	lpszMessage = StripHttpHeader(lpszResponse);
	WriteFile(lpParams->hFileOut, lpszMessage, 0);

	DestroyObject(hRequest);
	FreeMemory(lpszResponse);
}




INTERNAL_OPERATION
VOID
__Pause(
	_In_ 		LPPARAMS	lpParams
){ __LaunchRequest(lpParams, "/Api/Pause"); }




INTERNAL_OPERATION
VOID
__Resume(
	_In_ 		LPPARAMS	lpParams
){ __LaunchRequest(lpParams, "/Api/Resume"); }




INTERNAL_OPERATION
VOID
__Status(
	_In_ 		LPPARAMS	lpParams
){ __LaunchRequest(lpParams, "/Api/Status"); }




INTERNAL_OPERATION
VOID
__Ingress(
	_In_ 		LPPARAMS	lpParams
){ __LaunchRequest(lpParams, "/Api/GetIngressDevices"); }




INTERNAL_OPERATION
VOID
__Egress(
	_In_ 		LPPARAMS	lpParams
){ __LaunchRequest(lpParams, "/Api/GetEgressDevices"); }




INTERNAL_OPERATION
VOID
__IngressMulticast(
	_In_ 		LPPARAMS	lpParams
){ __LaunchRequest(lpParams, "/Api/GetIngressMulticastStreams"); }




INTERNAL_OPERATION
VOID
__EgressMulticast(
	_In_ 		LPPARAMS	lpParams
){ __LaunchRequest(lpParams, "/Api/GetEgressMulticastStreams"); }




INTERNAL_OPERATION
VOID
__ListAllEgress(
	_In_ 		LPPARAMS	lpParams
){ __LaunchRequest(lpParams, "/Api/ListAllSinks"); }




INTERNAL_OPERATION
VOID
__ListAllIngress(
	_In_ 		LPPARAMS	lpParams
){ __LaunchRequest(lpParams, "/Api/ListAllSources"); }




INTERNAL_OPERATION
VOID
__HandleInput(
	_In_ 		LPPARAMS	lpParams
){
	LPSTR lpszInput;
	DLPSTR dlpszSplit;
	UARCHLONG ualIndex;
	
	lpszInput = (LPSTR)lpParams->csInputBuffer;

	ReadLineFromFile(
		lpParams->hFileIn, 
		&lpszInput, 
		sizeof(lpParams->csInputBuffer),
		0
	);

	for (
		ualIndex = 0;
		ASCII_NULL_TERMINATOR != lpszInput[ualIndex];
		ualIndex++
	){
		if (ASCII_NULL_TERMINATOR == lpszInput[ualIndex])
		{ break; }

		if (ASCII_LINE_FEED == lpszInput[ualIndex])
		{ lpszInput[ualIndex] = ASCII_NULL_TERMINATOR; }
	}

	

	/* No Parameter Commands */
	if (
		0 == StringCompare("c", lpParams->csInputBuffer) ||
		0 == StringCompare("clear", lpParams->csInputBuffer)
	){ __Clear(lpParams); return; }
	
	else if (
		0 == StringCompare("q", lpParams->csInputBuffer) ||
		0 == StringCompare("quit", lpParams->csInputBuffer)
	){ PostQuitMessage(0); }

	else if (
		0 == StringCompare("v", lpParams->csInputBuffer) ||
		0 == StringCompare("version", lpParams->csInputBuffer)
	){ WriteFile(lpParams->hFileOut, "Pre-Alpha 0.0.1\n", 0); return; }
	
	else if (
		0 == StringCompare("p", lpParams->csInputBuffer) ||
		0 == StringCompare("pause", lpParams->csInputBuffer)
	){ __Pause(lpParams); return; }
	
	else if (
		0 == StringCompare("r", lpParams->csInputBuffer) ||
		0 == StringCompare("resume", lpParams->csInputBuffer)
	){ __Resume(lpParams); return; }

	else if (
		0 == StringCompare("C", lpParams->csInputBuffer) ||
		0 == StringCompare("config", lpParams->csInputBuffer)
	){
		CSTRING csBuffer[4096];
		CSTRING csHost[4096];

		ZeroMemory(csHost, sizeof(csHost));
		ZeroMemory(csBuffer, sizeof(csBuffer));

		ConvertIpv4AddressToString(lpParams->ip4Host, csHost, sizeof(csHost) - 1);

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"Host: %s\nPort: %hu\nSecret: %s\n",
			csHost,
			lpParams->usPort,
			lpParams->csSecret
		);

		WriteFile(lpParams->hFileOut, csBuffer, 0);
		return;
	}

	else if (
		0 == StringCompare("s", lpParams->csInputBuffer) ||
		0 == StringCompare("status", lpParams->csInputBuffer)
	){ __Status(lpParams); return;}
	
	else if (
		0 == StringCompare("i", lpParams->csInputBuffer) ||
		0 == StringCompare("ingress", lpParams->csInputBuffer)
	){ __Ingress(lpParams); return;}

	else if (
		0 == StringCompare("e", lpParams->csInputBuffer) ||
		0 == StringCompare("egress", lpParams->csInputBuffer)
	){ __Egress(lpParams); return;}
	
	else if (
		0 == StringCompare("im", lpParams->csInputBuffer) ||
		0 == StringCompare("ingress-multicast", lpParams->csInputBuffer)
	){ __IngressMulticast(lpParams); return;}
	
	else if (
		0 == StringCompare("em", lpParams->csInputBuffer) ||
		0 == StringCompare("egress-multicast", lpParams->csInputBuffer)
	){ __EgressMulticast(lpParams); return;}

	else if (
		0 == StringCompare("sinks", lpParams->csInputBuffer)
	){ __ListAllEgress(lpParams); return;}

	else if (
		0 == StringCompare("sources", lpParams->csInputBuffer)
	){ __ListAllIngress(lpParams); return;}


	/* One Or More Parameter Commands */

	StringSplit(lpParams->csInputBuffer, " ", &dlpszSplit);

	if (
		0 == StringCompare("h", dlpszSplit[0]) ||
		0 == StringCompare("host", dlpszSplit[0])
	){ 
		lpParams->ip4Host = CreateIpv4AddressFromString(dlpszSplit[1]); 
		PrintFormat("Set Host To: %s\n", dlpszSplit[1]);
	}

	else if (
		0 == StringCompare("P", dlpszSplit[0]) ||
		0 == StringCompare("port", dlpszSplit[0])
	){ 
		StringScanFormat(
			dlpszSplit[1],
			"%hu",
			&(lpParams->usPort)
		);

		PrintFormat("Set Port To: %hu\n", lpParams->usPort);
	}

	else if (
		0 == StringCompare("S", dlpszSplit[0]) ||
		0 == StringCompare("secret", dlpszSplit[0])
	){
		ZeroMemory(lpParams->csSecret, sizeof(lpParams->csSecret));

		StringScanFormat(
			dlpszSplit[1],
			"%s",
			lpParams->csSecret
		);
	}


	DestroySplitString(dlpszSplit);
}




LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	PARAMS parArgs;

	ZeroMemory(parArgs.csOutputBuffer, sizeof(parArgs.csOutputBuffer));

	parArgs.hFileIn = GetStandardIn();
	parArgs.hFileOut = GetStandardOut();
	parArgs.ip4Host.ulData = 0x100007F;
	parArgs.usPort = 8888;
	StringCopy(parArgs.csSecret, "SuperSecret");
	
	__PrintMenu(&parArgs);

	INFINITE_LOOP()
	{
		__HandleInput(&parArgs);


		ZeroMemory(parArgs.csInputBuffer, sizeof(parArgs.csInputBuffer));
		ZeroMemory(parArgs.csOutputBuffer, sizeof(parArgs.csOutputBuffer));
	}

	return 0;
}