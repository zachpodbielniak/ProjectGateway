#include <kore/kore.h>
#include <kore/http.h>

#include <PodNet/PodNet.h>
#include <PodNet/CString/CString.h>
#include <PodNet/CContainers/CContainers.h>
#include <PodNet/CNetworking/CIpv4.h>
#include <PodNet/CNetworking/CIpv6.h>
#include <PodNet/CNetworking/CSocket4.h>
#include <PodNet/CNetworking/CSocket6.h>
#include <PodNet/CSystem/CSystem.h>
#include <PodNet/CProcess/CProcess.h>


#define ERROR_WAIT_COUNT	0x64



typedef struct http_request 		HTTP_REQUEST, *LPHTTP_REQUEST;


typedef struct __CONFIG
{
	HANDLE 			hConfigFile;	
	HANDLE 			hThread;
	HANDLE 			hSocket;
	HANDLE 			hLock;
	HANDLE 			hProcess;

	LPSTR 			lpszTheSecret;
	
	IPV4_ADDRESS		ip4Server;
	USHORT			usPort;
} CONFIG, *LPCONFIG;



GLOBAL_VARIABLE LPCONFIG lpcApp = NULLPTR;





INTERNAL_OPERATION
LPVOID
__ClientKeepAliveProc(
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(lpParam);
	CHAR csCommand[0x10];
	
	ZeroMemory(csCommand, sizeof(csCommand));
	StringCopy(csCommand, "KeepAlive\n");

	INFINITE_LOOP()
	{
		WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);
		SocketSend4(lpcApp->hSocket, csCommand, StringLength(csCommand));
		ReleaseSingleObject(lpcApp->hLock);
		Sleep(15000);
	}

	return NULLPTR;
}




INTERNAL_OPERATION
BOOL
__ConnectionPrereq(
	VOID
){
	HANDLE hTemp;
	CSTRING csBuffer[0x1000];

	ZeroMemory(csBuffer, sizeof(csBuffer));

	if (NULLPTR == lpcApp->hLock)
	{
		hTemp = CreateCriticalSection();
		if (FALSE == InterlockedAcquireExchangeHappened(&(lpcApp->hLock),  hTemp))
		{ DestroyObject(hTemp); }
	}

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);
	
	if (NULLPTR == lpcApp->hProcess)
	{ 
		lpcApp->hProcess = CreateProcess("../bin/CServer", "", FALSE); 
		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			ANSI_BLUE_BOLD					\
			"Created SmoofServer Process: %d\n"		\
			ANSI_RESET,
			GetProcessIdentifer(lpcApp->hProcess)
		);
		WriteFile(GetStandardOut(), csBuffer, 0);
	}

	Sleep(10);

	if (NULLPTR == lpcApp->hSocket)
	{
		lpcApp->hSocket = CreateSocketWithAddress4(lpcApp->usPort, lpcApp->ip4Server);
		if (FALSE == SocketConnect4(lpcApp->hSocket))
		{
			DestroyObject(lpcApp->hSocket);
			lpcApp->hSocket = NULL_OBJECT;
			ReleaseSingleObject(lpcApp->hLock);
			return FALSE;
		}
	}

	if (NULLPTR == lpcApp->hThread)
	{ lpcApp->hThread = CreateThread(__ClientKeepAliveProc, NULLPTR, NULLPTR); }

	ReleaseSingleObject(lpcApp->hLock);
	return TRUE;
}




/* Parses the config files */
_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__ParseConfig(
	VOID
){
	CSTRING csBuffer[0x1000];
	CSTRING csParse[0x1000];
	LPSTR lpszBuffer;
	UARCHLONG ualArgs;
	DLPSTR dlpszSplit;

	ZeroMemory(csBuffer, sizeof(csBuffer));
	ZeroMemory(csParse, sizeof(csParse));

	lpcApp->hConfigFile = OpenFile("SmoofConfig", FILE_PERMISSION_READ, 0);
	if (NULL_OBJECT == lpcApp->hConfigFile)
	{
		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			ANSI_RED_BOLD 					\
			"NO CONFIG FILE WAS AVAILABLE!\n"		\
			"Create a file called SmoofConfig\n\n"		\
			ANSI_RESET
		);

		WriteFile(GetStandardError(), csBuffer, 0);
		PostQuitMessage(1);
	}

	lpszBuffer = csBuffer;

	/* Parse the config file */
	while (
		-1 != ReadLineFromFile(
			lpcApp->hConfigFile, 
			&lpszBuffer, 
			sizeof(csBuffer) - 1, 
			0
		)
	){
		ualArgs = StringSplit(lpszBuffer, "=", &dlpszSplit);

		if (0 == StringCompare("Port", dlpszSplit[0]))
		{
			if (1 == ualArgs)
			{ continue; }

			StringScanFormat(
				dlpszSplit[1],
				"%hu",
				&(lpcApp->usPort)
			);
		}

		else if (0 == StringCompare("Secret", dlpszSplit[0]))
		{
			if (1 == ualArgs)
			{ continue; }

			ZeroMemory(csParse, sizeof(csParse));

			StringScanFormat(
				dlpszSplit[1],
				"%s",
				csParse
			);

			lpcApp->lpszTheSecret = GlobalAllocAndZero(StringLength(csParse) + 1);
			StringCopy(lpcApp->lpszTheSecret, csParse);
		}

		else if (0 == StringCompare("Server4", dlpszSplit[0]))
		{
			if (1 == ualArgs)
			{ continue; }

			lpcApp->ip4Server = CreateIpv4AddressFromString(dlpszSplit[1]);
		}
		
		ZeroMemory(csBuffer, sizeof(csBuffer));
	}

	return TRUE;
}




/* This is the GCC constructor "OnLoad" Function */
/* This is called when the library is loaded */
INTERNAL_OPERATION
VOID
__attribute__((constructor))
__Constructor(
	VOID
){
	CSTRING csBuffer[0x1000];

	ZeroMemory(csBuffer, sizeof(csBuffer));

	lpcApp = GlobalAllocAndZero(sizeof(CONFIG));
	if (NULLPTR == lpcApp)
	{ PostQuitMessage(1); }
	
	__ParseConfig();

	while (FALSE == __ConnectionPrereq()){} 
	

	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		ANSI_BLUE_BOLD
		"\nSuccessfully Connected To SmoofAudioServer\n" 	\
		"\nConnected On Port %hu\n" 				\
		"Secret Is: %s\n\n"					\
		ANSI_RESET,
		lpcApp->usPort,
		lpcApp->lpszTheSecret
	);

	WriteFile(GetStandardError(), csBuffer, 0);
	return;
}




/* Runs on termination of process */
INTERNAL_OPERATION
VOID
__attribute__((destructor))
__Destructor(
	VOID
){
	WriteFile(GetStandardError(), "Killing Process!\n\n", 0);
	/* KillProcess(lpcApp->hProcess, SIGTERM); */
}




LONG
PageIndex(
	_In_ 	LPHTTP_REQUEST 		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CSTRING csResponse[0x1000];
	LPSTR lpszSecret;
	ZeroMemory(csResponse, sizeof(csResponse));

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);
	
	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{ StringCopy(csResponse, "GOOD!\n"); }
	else
	{ StringCopy(csResponse, "BAD!\n"); }	

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return (KORE_RESULT_OK);
}




LONG
PagePause(
	_In_ 	LPHTTP_REQUEST 		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;
	
	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));
	StringCopy(csCommand, "Pause\n");

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);
	
	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{ 
		SocketSend4(lpcApp->hSocket, csCommand, sizeof(csCommand));
		SocketReceive4(lpcApp->hSocket, csResponse, sizeof(csResponse));
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;
}



LONG
PageResume(
	_In_ 	LPHTTP_REQUEST 		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));
	StringCopy(csCommand, "Resume\n");

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{ 
		SocketSend4(lpcApp->hSocket, csCommand, sizeof(csCommand));
		SocketReceive4(lpcApp->hSocket, csResponse, sizeof(csResponse));
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;
}




LONG
PageStatus(
	_In_ 	LPHTTP_REQUEST 		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;
	

	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));
	StringCopy(csCommand, "Status\n");

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{
		SocketSend4(lpcApp->hSocket, csCommand, sizeof(csCommand));
		SocketReceive4(lpcApp->hSocket, csResponse, sizeof(csResponse));
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;
}




LONG 
PageIngressDevices(
	_In_ 	LPHTTP_REQUEST 		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;
	

	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));
	StringCopy(csCommand, "GetIngressDevices\n");

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{
		SocketSend4(lpcApp->hSocket, csCommand, sizeof(csCommand));
		SocketReceive4(lpcApp->hSocket, csResponse, sizeof(csResponse));
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;
}




LONG 
PageEgressDevices(
	_In_ 	LPHTTP_REQUEST 		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;
	

	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));
	StringCopy(csCommand, "GetEgressDevices\n");

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{
		SocketSend4(lpcApp->hSocket, csCommand, sizeof(csCommand));
		SocketReceive4(lpcApp->hSocket, csResponse, sizeof(csResponse));
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;
}




LONG 
PageIngressMulticast(
	_In_ 	LPHTTP_REQUEST 		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;
	

	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));
	StringCopy(csCommand, "GetIngressMulticastStreams\n");

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{
		SocketSend4(lpcApp->hSocket, csCommand, sizeof(csCommand));
		SocketReceive4(lpcApp->hSocket, csResponse, sizeof(csResponse));
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;
}




LONG 
PageEgressMulticast(
	_In_ 	LPHTTP_REQUEST 		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;
	

	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));
	StringCopy(csCommand, "GetEgressMulticastStreams\n");

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{
		SocketSend4(lpcApp->hSocket, csCommand, sizeof(csCommand));
		SocketReceive4(lpcApp->hSocket, csResponse, sizeof(csResponse));
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;
}



/* Used to add a Unicast Ingress device */
LONG 
PageAddIngressUnicast(
	_In_ 	LPHTTP_REQUEST 		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;
	LPSTR lpszAddress;
	LPSTR lpszPort;
	

	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);
	http_argument_get_string(lphrRequest, "Address", &lpszAddress);
	http_argument_get_string(lphrRequest, "Port", &lpszPort);

	StringPrintFormatSafe(
		csCommand,
		sizeof(csCommand) - 1,
		"AddIngressUnicast %s?%s\n",
		lpszAddress,
		lpszPort
	);

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{
		SocketSend4(lpcApp->hSocket, csCommand, sizeof(csCommand));
		SocketReceive4(lpcApp->hSocket, csResponse, sizeof(csResponse));
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;
}




/* Used to add a Unicast Egress device */
LONG 
PageAddEgressUnicast(
	_In_ 	LPHTTP_REQUEST 		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;
	LPSTR lpszAddress;
	LPSTR lpszPort;
	

	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);
	http_argument_get_string(lphrRequest, "Address", &lpszAddress);
	http_argument_get_string(lphrRequest, "Port", &lpszPort);

	StringPrintFormatSafe(
		csCommand,
		sizeof(csCommand) - 1,
		"AddEgressUnicast %s?%s\n",
		lpszAddress,
		lpszPort
	);

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{
		SocketSend4(lpcApp->hSocket, csCommand, sizeof(csCommand));
		SocketReceive4(lpcApp->hSocket, csResponse, sizeof(csResponse));
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;
}




LONG
PageListAllSinks(
	_In_ 	LPHTTP_REQUEST		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;


	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{
		HANDLE hProcess, hFile;
		LPSTR lpszOutput;

		hProcess = CreateProcess(
			"pactl", 
			"list short sinks", 
			FALSE
		);		

		hFile = GetProcessOutput(hProcess);
		lpszOutput = LocalAllocAndZero(sizeof(csResponse));

		WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
		
		while (0 < ReadLineFromFile(hFile, &lpszOutput, sizeof(csResponse) - 1, 0))
		{
			StringConcatenateSafe(
				csResponse,
				lpszOutput,
				sizeof(csResponse) - 1		
			);
		}


		FreeMemory(lpszOutput);
		DestroyObject(hProcess);
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;

}




LONG
PageListAllSources(
	_In_ 	LPHTTP_REQUEST		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;


	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{
		HANDLE hProcess, hFile;
		LPSTR lpszOutput;

		hProcess = CreateProcess(
			"pactl", 
			"list short sources", 
			FALSE
		);		

		hFile = GetProcessOutput(hProcess);
		lpszOutput = LocalAllocAndZero(sizeof(csResponse));

		WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
	
		while (0 < ReadLineFromFile(hFile, &lpszOutput, sizeof(csResponse) - 1, 0))
		{
			StringConcatenateSafe(
				csResponse,
				lpszOutput,
				sizeof(csResponse) - 1		
			);
		}

		FreeMemory(lpszOutput);
		DestroyObject(hProcess);
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;

}




LONG
PageBluetoothScan(
	_In_ 	LPHTTP_REQUEST		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret;


	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{
		HANDLE hProcess, hFile;
		LPSTR lpszOutput;

		hProcess = CreateProcess(
			"python", 
			"../Connect/blu-dis.py", 
			FALSE
		);		

		hFile = GetProcessOutput(hProcess);
		lpszOutput = LocalAllocAndZero(sizeof(csResponse));

		WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
		ReadLineFromFile(hFile, &lpszOutput, sizeof(csResponse) - 1, 0);

		StringCopy(
			csResponse,
			lpszOutput			
		);

		FreeMemory(lpszOutput);
		DestroyObject(hProcess);
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;
}




LONG
PageBluetoothConnect(
	_In_ 	LPHTTP_REQUEST		lphrRequest
){
	if (FALSE == __ConnectionPrereq())
	{ return KORE_RESULT_RETRY; }

	CHAR csCommand[0x40];
	CHAR csResponse[0x1000];
	LPSTR lpszSecret, lpszMac;


	ZeroMemory(csCommand, sizeof(csCommand));
	ZeroMemory(csResponse, sizeof(csResponse));

	http_populate_get(lphrRequest);
	http_argument_get_string(lphrRequest, "Secret", &lpszSecret);
	http_argument_get_string(lphrRequest, "Mac", &lpszMac);

	WaitForSingleObject(lpcApp->hLock, INFINITE_WAIT_TIME);

	if (0 == StringCompare(lpcApp->lpszTheSecret, lpszSecret))
	{
		HANDLE hProcess, hFile;
		CSTRING csTemp[0x40];
		LPSTR lpszOutput;

		ZeroMemory(csTemp, sizeof(csTemp));

		StringCopySafe(
			csTemp,
			"../Connect/blu-con.py ",
			sizeof(csTemp) - 1
		);

		StringConcatenateSafe(
			csTemp,
			lpszMac,
			sizeof(csTemp) - 1
		);

		hProcess = CreateProcess(
			"python", 
			csTemp, 
			FALSE
		);		

		hFile = GetProcessOutput(hProcess);
		lpszOutput = LocalAllocAndZero(sizeof(csResponse));

		WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
		ReadLineFromFile(hFile, &lpszOutput, sizeof(csResponse) - 1, 0);

		StringCopy(
			csResponse,
			lpszOutput			
		);

		FreeMemory(lpszOutput);
		DestroyObject(hProcess);
	}
	else
	{ StringCopy(csResponse, "Authentication Error!\n"); }

	ReleaseSingleObject(lpcApp->hLock);

	http_response(lphrRequest, 200, csResponse, StringLength(csResponse));
	return KORE_RESULT_OK;
}
