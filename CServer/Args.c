#ifndef ARGS_C
#define ARGS_C


#include <PodNet/Ascii.h>
#include <PodNet/CLog/CLog.h>
#include <PodNet/CString/CString.h>
#include "../CAudioRouter/CAudioRouter.h"


#define MAX_DEVICE_NAME			0x100


typedef struct __ARGUMENTS
{
	HANDLE 		hAudioRouter;
	LONG 		lArgCount;
	DLPSTR		dlpszArgValues;

	SAMPLE_FORMAT	sfFormat;
	ULONG 		ulRate;
	BYTE 		byChannels;

	LPSTR		lpszConfigFile;
	LPSTR 		lpszLogFile;

	BOOL		bDaemonize;

	HANDLE 		hConfigFile;
	HANDLE 		hLogFile;

	HVECTOR		hvIngressMulticast;
	HVECTOR 	hvEgressMulticast;
	HVECTOR 	hvIngressDevice;
	HVECTOR 	hvEgressDevice;

	USHORT 		usIngressPort;
	USHORT 		usEgressPort;

	IPV4_ADDRESS	ip4Bind;
	USHORT 		usBindPort;

	
} ARGUMENTS, *LPARGUMENTS;




VOID
PrintHelp(
	VOID
){
	PrintFormat("\nProject Gateway Server\n");
	PrintFormat("----------------------\n\n");

	PrintFormat("This tool is used to act as the server for the entire Project Gateway Eco-System.\n");
	PrintFormat("This will be the tool that reads in the one or more ingress inputs and output that\n");
	PrintFormat("to one or more egress devices.\n");

	PrintFormat("\nUsage:\n");
	PrintFormat("\tpgs [-kcliomM]\n");
	PrintFormat("\n\t-h, --help : Print this message.\n");
	PrintFormat("\t-k, --kill : Kill an already running version of this server.\n");
	PrintFormat("\t-c, --config : Specify a config file.\n");
	PrintFormat("\t-l, --log : Specify a log file.\n");

	PrintFormat("\nAudio Settings:\n");
	PrintFormat("\tBy default, the settings are 2 channel @ 44.1khz, with a sample size of\n");
	PrintFormat("\tS16LE (Signed 16 Bit Little Endian).\n");
	PrintFormat("\n\t-c, --channels : The number of channels.\n");
	PrintFormat("\t-r, --rate : The audio rate.\n");
	PrintFormat("\t-s, --sample : The sample size.\n");

	PrintFormat("\nPulse Audio Devices:\n");
	PrintFormat("\tThis can be the full name, or the index.\n");
	PrintFormat("\n\t-i, --ingress-device : Specify an ingress stream (Pulse Audio Device).\n");
	PrintFormat("\t-o, --egress-device : Specify an egress stream (Pulse Audio Device).\n");

	PrintFormat("\nMulticasting:\n");
	PrintFormat("\tThis can be IPv4 or IPv6 addresses. Use the format GROUP@BIND?PORT.\n");
	PrintFormat("\tFor example, 239.255.175.222@0.0.0.0?57007. This will bind to all interfaces on\n");
	PrintFormat("\tport 57007, and join the multicast group 239.255.175.222.\n");
	PrintFormat("\n\t-m, --ingress-multicast: Specify a multicast ingress input. GROUP@BIND:PORT.\n");
	PrintFormat("\t-M, --egress-multicast : Specify a multicast egress output. GROUP@BIND:PORT.\n");
	
	PrintFormat("\nServer Settings:\n");
	PrintFormat("\tBy default, the server binds to ::1 on port 9001 (Its Over 9000!).\n");
	PrintFormat("\tThis is done this way, as we can use a Kore Web Server as a proxy for\n");
	PrintFormat("\tAPI calls. Which will provide a very simple way to include encryption.\n");
	PrintFormat("\n\t-b, --bind : Bind address for server.\n");
	PrintFormat("\t-p, --port : TCP port server will listen on.\n");

	PrintFormat("\n");
	return;
}




_Success_(return != FALSE, _Non_Locking_)
BOOL
ReadInArguments(
	_In_		LPARGUMENTS	lpargParams
){
	UARCHLONG ualIndex;
	
	for (
		ualIndex = 1;
		ualIndex <= (UARCHLONG)lpargParams->lArgCount;
		ualIndex++
	){
		/* Print Help */
		if (
			0 == StringCompare("-h", lpargParams->dlpszArgValues[ualIndex]) ||
			0 == StringCompare("--help", lpargParams->dlpszArgValues[ualIndex])
		){
			PrintHelp();
			PostQuitMessage(0);
		}


		/* Specify Config File */
		if (
			(
				0 == StringCompare("-c", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--config", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex < (UARCHLONG)(lpargParams->lArgCount - 1)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }

			lpargParams->lpszConfigFile = lpargParams->dlpszArgValues[ualIndex];
		}

		/* Log File */
		if (
			(
				0 == StringCompare("-l", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--log", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex < (UARCHLONG)(lpargParams->lArgCount - 1)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }

			if (NULL_OBJECT == lpargParams->hLogFile)
			{ continue; }
			
			lpargParams->lpszLogFile = lpargParams->dlpszArgValues[ualIndex];

			lpargParams->hLogFile = CreateLog(lpargParams->lpszLogFile, 0);
			if (NULL_OBJECT == lpargParams->hLogFile)
			{
				WriteFile(GetStandardError(), "Could Not Create Log File\n", 0);
				PostQuitMessage(1);
			}
		}


		/* Ingress Multicast */
		if (
			(
				0 == StringCompare("-m", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--ingress-multicast", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex <= (UARCHLONG)(lpargParams->lArgCount)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }
			
			CHAR csGroupAddress[42];
			CHAR csBindAddress[42];
			CHAR csPort[8];
			DLPSTR dlpszSplit;
			USHORT usPort;

			ZeroMemory(csGroupAddress, sizeof(csGroupAddress));
			ZeroMemory(csBindAddress, sizeof(csBindAddress));
			ZeroMemory(csPort, sizeof(csPort));


			if (3 != StringSplit(
				lpargParams->dlpszArgValues[ualIndex],
				"@?",
				&dlpszSplit
			)){ PostQuitMessage(1); }

			StringScanFormat(
				dlpszSplit[2],
				"%hu",
				&usPort
			);

			AudioRouterAddIngressMulticastRouting(
				lpargParams->hAudioRouter,
				dlpszSplit[0], 
				dlpszSplit[1],
				usPort
			);
		}


		/* Egress Multicast */
		if (
			(
				0 == StringCompare("-M", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--egress-multicast", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex <= (UARCHLONG)(lpargParams->lArgCount - 1)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }

			CHAR csGroupAddress[42];
			CHAR csBindAddress[42];
			CHAR csPort[8];
			DLPSTR dlpszSplit;
			USHORT usPort;

			ZeroMemory(csGroupAddress, sizeof(csGroupAddress));
			ZeroMemory(csBindAddress, sizeof(csBindAddress));
			ZeroMemory(csPort, sizeof(csPort));


			if (3 != StringSplit(
				lpargParams->dlpszArgValues[ualIndex],
				"@?",
				&dlpszSplit
			)){ PostQuitMessage(1); }

			StringScanFormat(
				dlpszSplit[2],
				"%hu",
				&usPort
			);

			AudioRouterAddEgressMulticastRouting(
				lpargParams->hAudioRouter,
				dlpszSplit[0], 
				usPort
			);

			DestroySplitString(dlpszSplit);
		}
		
		
		/* Ingress Uniicast */
		if (
			(
				0 == StringCompare("-u", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--ingress-unicast", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex <= (UARCHLONG)(lpargParams->lArgCount)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }
			
			CHAR csBindAddress[42];
			CHAR csPort[8];
			DLPSTR dlpszSplit;
			USHORT usPort;

			ZeroMemory(csBindAddress, sizeof(csBindAddress));
			ZeroMemory(csPort, sizeof(csPort));


			if (2 != StringSplit(
				lpargParams->dlpszArgValues[ualIndex],
				"?",
				&dlpszSplit
			)){ PostQuitMessage(1); }

			StringScanFormat(
				dlpszSplit[1],
				"%hu",
				&usPort
			);

			AudioRouterAddIngressUnicastDevice(
				lpargParams->hAudioRouter,
				dlpszSplit[0],
				usPort
			);
		}
		
		
		/* Egress Uniicast */
		if (
			(
				0 == StringCompare("-E", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--egress-unicast", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex <= (UARCHLONG)(lpargParams->lArgCount)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }
			
			CHAR csBindAddress[42];
			CHAR csPort[8];
			DLPSTR dlpszSplit;
			USHORT usPort;

			ZeroMemory(csBindAddress, sizeof(csBindAddress));
			ZeroMemory(csPort, sizeof(csPort));


			if (2 != StringSplit(
				lpargParams->dlpszArgValues[ualIndex],
				"?",
				&dlpszSplit
			)){ PostQuitMessage(1); }

			StringScanFormat(
				dlpszSplit[1],
				"%hu",
				&usPort
			);

			AudioRouterAddEgressUnicastDevice(
				lpargParams->hAudioRouter,
				dlpszSplit[0],
				usPort
			);
		}


		/* Ingress Device */	
		if (
			(
				0 == StringCompare("-i", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--ingress", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--ingress-device", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex <= (UARCHLONG)(lpargParams->lArgCount - 1)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }

			if (FALSE == AudioRouterAddIngressDevice(
				lpargParams->hAudioRouter,
				lpargParams->dlpszArgValues[ualIndex],
				lpargParams->sfFormat,
				lpargParams->ulRate,
				lpargParams->byChannels,
				NULLPTR
			)){ PostQuitMessage(1); }
		}


		/* Egress Device */
		if (
			(
				0 == StringCompare("-o", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--egress", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--egress-device", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex <= (UARCHLONG)(lpargParams->lArgCount - 1)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }

			if (FALSE == AudioRouterAddEgressDevice(
				lpargParams->hAudioRouter,
				lpargParams->dlpszArgValues[ualIndex],
				lpargParams->sfFormat,
				lpargParams->ulRate,
				lpargParams->byChannels,
				NULLPTR
			)){ PostQuitMessage(1); }
		}


		/* Sample Format */
		if (
			(
				0 == StringCompare("-s", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--sample-format", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex <= (UARCHLONG)(lpargParams->lArgCount - 1)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }

			if (0 == StringCompare("U8", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_U8; }
			else if (0 == StringCompare("ALAW", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_ALAW; }
			else if (0 == StringCompare("ULAW", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_ULAW; }
			else if (0 == StringCompare("S16LE", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_S16LE; }
			else if (0 == StringCompare("S16BE", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_S16BE; }
			else if (0 == StringCompare("FLOAT32LE", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_FLOAT32LE; }
			else if (0 == StringCompare("FLOAT32BE", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_FLOAT32BE; }
			else if (0 == StringCompare("S32LE", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_S32LE; }
			else if (0 == StringCompare("S32BE", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_S32BE; }
			else if (0 == StringCompare("S24LE", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_S24LE; }
			else if (0 == StringCompare("S24BE", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_S24BE; }
			else if (0 == StringCompare("S24_32LE", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_S24_32LE; }
			else if (0 == StringCompare("S24_32BE", lpargParams->dlpszArgValues[ualIndex]))
			{ lpargParams->sfFormat = PA_SAMPLE_S24_32BE; }
			else
			{ lpargParams->sfFormat = PA_SAMPLE_S16LE; }

		}


		/* Rate */
		if (
			(
				0 == StringCompare("-r", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--rate", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex <= (UARCHLONG)(lpargParams->lArgCount - 1)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }

			if (1 != StringScanFormat(
				lpargParams->dlpszArgValues[ualIndex],
				"%u",
				&(lpargParams->ulRate)
			)){ lpargParams->ulRate = 44100; }
		}


		/* Channels */
		if (
			(
				0 == StringCompare("-c", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--channels", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex <= (UARCHLONG)(lpargParams->lArgCount - 1)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }

			if (1 != StringScanFormat(
				lpargParams->dlpszArgValues[ualIndex],
				"%hhu",
				&(lpargParams->byChannels)
			)){ lpargParams->byChannels = 2; }
		}

		
		/* Binding IP Address */
		if (
			(
				0 == StringCompare("-b", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--bind", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex <= (UARCHLONG)(lpargParams->lArgCount - 1)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }

			lpargParams->ip4Bind = CreateIpv4AddressFromString(lpargParams->dlpszArgValues[ualIndex]);
		}
		
		
		/* Binding Port */
		if (
			(
				0 == StringCompare("-p", lpargParams->dlpszArgValues[ualIndex]) ||
				0 == StringCompare("--port", lpargParams->dlpszArgValues[ualIndex])
			) && ualIndex <= (UARCHLONG)(lpargParams->lArgCount - 1)
		){
			ualIndex++;
			if (ASCII_HYPHEN == lpargParams->dlpszArgValues[ualIndex][0])
			{ continue; }

			if (1 != StringScanFormat(
				lpargParams->dlpszArgValues[ualIndex],
				"%hu",
				&(lpargParams->usBindPort)
			)){ lpargParams->usBindPort = 6969; }
		}


		/* Kill Existing */
		if (
			0 == StringCompare("-k", lpargParams->dlpszArgValues[ualIndex]) ||
			0 == StringCompare("--kill", lpargParams->dlpszArgValues[ualIndex])
		){
			CHAR csBuffer[0x400];
			ZeroMemory(csBuffer, sizeof(csBuffer));

			StringCopySafe(
				csBuffer,
				"killall -9 ",
				sizeof(csBuffer)
			);

			StringConcatenate(
				csBuffer,
				lpargParams->dlpszArgValues[0]
			);

			/* Run And Log Results */
			if (0 == system(csBuffer))
			{
				WriteFile(GetStandardError(), "Successfully Killed Existing Process.\n", 0);
				if (NULL_OBJECT != lpargParams->hLogFile)
				{
					WriteLog(
						lpargParams->hLogFile,
						"Successfully Killed Existing Process.",
						LOG_INFO,
						0
					);
				}
			}
			else
			{
				WriteFile(GetStandardError(), "Could Not Kill Existing Process. Is There One Running?\n", 0);
				if (NULL_OBJECT != lpargParams->hLogFile)
				{
					WriteLog(
						lpargParams->hLogFile,
						"Could Not Kill Existing Process. Is There One Running?",
						LOG_INFO,
						0
					);
				}
			}
		}
	}

	return TRUE;
}

#endif