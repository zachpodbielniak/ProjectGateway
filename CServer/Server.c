#ifndef SERVER_C
#define SERVER_C


#include <PodNet/Ascii.h>
#include <PodNet/CLog/CLog.h>
#include <PodNet/CString/CString.h>
#include "../CAudioRouter/CAudioRouter.h"

#include "Args.c"

#define SERVER_TCP_PORT		6969
#define CLIENT_TIMEOUT		0x4000


typedef struct __SERVER
{
	HANDLE 		hSocket;
	HANDLE 		hServerThread;
	HANDLE 		hLock;
	LPARGUMENTS	lpArgs;
} SERVER, *LPSERVER;


typedef struct __CLIENT_PARAM
{
	HANDLE 		hClient;
	HANDLE 		hThread;
	LPSERVER 	lpServer;
	ULONG 		ulTimeout;
} CLIENT_PARAM, *LPCLIENT_PARAM;



INTERNAL_OPERATION
BOOL
__HandleCommand(
	_In_ 		LPSERVER 		lpServer,
	_In_Z_ 		LPSTR 			lpszCommand,
	_Out_Z_		LPSTR 			lpszResponse,
	_In_ 		UARCHLONG 		ualResponseSize
){
	DLPSTR dlpszSplit;
	BOOL bRet;

	ZeroMemory(lpszResponse, ualResponseSize);

	bRet = FALSE;
	StringSplit((LPCSTR)lpszCommand, " ", &dlpszSplit);

	if (0 == StringCompare("KeepAlive", dlpszSplit[0]))
	{ bRet = TRUE; }
	
	else if (0 == StringCompare("Kill", dlpszSplit[0]))
	{ PostQuitMessage(0); }

	else if (0 == StringCompare("AddIngress", dlpszSplit[0]))
	{
		bRet = AudioRouterAddIngressDevice(
			lpServer->lpArgs->hAudioRouter,
			dlpszSplit[1],
			lpServer->lpArgs->sfFormat,
			lpServer->lpArgs->ulRate,
			lpServer->lpArgs->byChannels,
			NULLPTR
		);

		if (FALSE != bRet)
		{ 
			StringCopy(
				lpszResponse,
				"TRUE\n"
			);
		}
		else
		{
			StringCopy(
				lpszResponse,
				"FALSE\n"
			);
		}
	}

	else if (0 == StringCompare("RemoveIngress", dlpszSplit[0]))
	{
		bRet = AudioRouterRemoveIngressDevice(
			lpServer->lpArgs->hAudioRouter,
			dlpszSplit[1]
		);

		if (FALSE != bRet)
		{ 
			StringCopy(
				lpszResponse,
				"TRUE\n"
			);
		}
		else
		{
			StringCopy(
				lpszResponse,
				"FALSE\n"
			);
		}
	}

	else if (0 == StringCompare("AddEgress", dlpszSplit[0]))
	{
		bRet = AudioRouterAddEgressDevice(
			lpServer->lpArgs->hAudioRouter,
			dlpszSplit[1],
			lpServer->lpArgs->sfFormat,
			lpServer->lpArgs->ulRate,
			lpServer->lpArgs->byChannels,
			NULLPTR
		);

		if (FALSE != bRet)
		{ 
			StringCopy(
				lpszResponse,
				"TRUE\n"
			);
		}
		else
		{
			StringCopy(
				lpszResponse,
				"FALSE\n"
			);
		}
	}

	else if (0 == StringCompare("RemoveEgress", dlpszSplit[0]))
	{
		bRet = AudioRouterRemoveEgressDevice(
			lpServer->lpArgs->hAudioRouter,
			dlpszSplit[1]
		);

		if (FALSE != bRet)
		{ 
			StringCopy(
				lpszResponse,
				"TRUE\n"
			);
		}
		else
		{
			StringCopy(
				lpszResponse,
				"FALSE\n"
			);
		}
	}

	else if (0 == StringCompare("AddIngressMulticast", dlpszSplit[0]))
	{
		DLPSTR dlpszMulticast;
		USHORT usPort;

		StringSplit(
			dlpszSplit[1],
			"@?",
			&dlpszMulticast
		);

		usPort = StringScanFormat(
			dlpszMulticast[2],
			"%hu",
			&usPort
		);

		bRet = AudioRouterAddIngressMulticastRouting(
			lpServer->lpArgs->hAudioRouter,
			dlpszMulticast[0],
			dlpszMulticast[1],
			usPort
		);

		DestroySplitString(dlpszMulticast);

		if (FALSE != bRet)
		{ 
			StringCopy(
				lpszResponse,
				"TRUE\n"
			);
		}
		else
		{
			StringCopy(
				lpszResponse,
				"FALSE\n"
			);
		}
	}
	
	
	else if (0 == StringCompare("AddIngressUnicast", dlpszSplit[0]))
	{
		DLPSTR dlpszUnicast;
		USHORT usPort;

		StringSplit(
			dlpszSplit[1],
			"?",
			&dlpszUnicast
		);

		usPort = StringScanFormat(
			dlpszUnicast[1],
			"%hu",
			&usPort
		);

		bRet = AudioRouterAddIngressUnicastDevice(
			lpServer->lpArgs->hAudioRouter,
			dlpszUnicast[0],
			usPort
		);

		DestroySplitString(dlpszUnicast);

		if (FALSE != bRet)
		{ 
			StringCopy(
				lpszResponse,
				"TRUE\n"
			);
		}
		else
		{
			StringCopy(
				lpszResponse,
				"FALSE\n"
			);
		}
	}

	else if (0 == StringCompare("AddEgressUnicast", dlpszSplit[0]))
	{
		DLPSTR dlpszUnicast;
		USHORT usPort;

		StringSplit(
			dlpszSplit[1],
			"?",
			&dlpszUnicast
		);

		usPort = StringScanFormat(
			dlpszUnicast[1],
			"%hu",
			&usPort
		);

		bRet = AudioRouterAddEgressUnicastDevice(
			lpServer->lpArgs->hAudioRouter,
			dlpszUnicast[0],
			usPort
		);

		DestroySplitString(dlpszUnicast);

		if (FALSE != bRet)
		{ 
			StringCopy(
				lpszResponse,
				"TRUE\n"
			);
		}
		else
		{
			StringCopy(
				lpszResponse,
				"FALSE\n"
			);
		}
	}


	else if (0 == StringCompare("AddEgressMulticast", dlpszSplit[0]))
	{
		DLPSTR dlpszMulticast;
		USHORT usPort;

		StringSplit(
			dlpszSplit[1],
			"@?",
			&dlpszMulticast
		);

		usPort = StringScanFormat(
			dlpszMulticast[2],
			"%hu",
			&usPort
		);

		bRet = AudioRouterAddEgressMulticastRouting(
			lpServer->lpArgs->hAudioRouter,
			dlpszMulticast[0],
			usPort
		);

		DestroySplitString(dlpszMulticast);

		if (FALSE != bRet)
		{ 
			StringCopy(
				lpszResponse,
				"TRUE\n"
			);
		}
		else
		{
			StringCopy(
				lpszResponse,
				"FALSE\n"
			);
		}
	}

	else if (0 == StringCompare("Pause", dlpszSplit[0]))
	{ 
		bRet = AudioRouterPause(lpServer->lpArgs->hAudioRouter); 
	
		if (FALSE != bRet)
		{ 
			StringCopy(
				lpszResponse,
				"TRUE\n"
			);
		}
		else
		{
			StringCopy(
				lpszResponse,
				"FALSE\n"
			);
		}
	}

	else if (0 == StringCompare("Resume", dlpszSplit[0]))
	{ 
		bRet = AudioRouterResume(lpServer->lpArgs->hAudioRouter); 

		if (FALSE != bRet)
		{ 
			StringCopy(
				lpszResponse,
				"TRUE\n"
			);
		}
		else
		{
			StringCopy(
				lpszResponse,
				"FALSE\n"
			);
		}
	}

	else if (0 == StringCompare("Status", dlpszSplit[0]))
	{
		bRet = AudioRouterGetStatus(
			lpServer->lpArgs->hAudioRouter,
			lpszResponse,
			ualResponseSize
		);
	}

	else if (0 == StringCompare("GetIngressDevices", dlpszSplit[0]))
	{
		DLPSTR dlpszDevices;
		UARCHLONG ualIndex;

		dlpszDevices = AudioRouterGetIngressDevices(lpServer->lpArgs->hAudioRouter);

		if (NULLPTR != dlpszDevices)
		{
			for (
				ualIndex = 0;
				FALSE == STRING_SPLIT_NULL_TERMINATOR(dlpszDevices[ualIndex]);
				ualIndex++
			){
				if (ASCII_NULL_TERMINATOR == dlpszDevices[ualIndex][0])
				{ continue; }

				StringConcatenateSafe(
					lpszResponse,
					dlpszDevices[ualIndex],
					ualResponseSize
				);

				StringConcatenateSafe(
					lpszResponse,
					"\n",
					ualResponseSize
				);
			}
		}

		DestroySplitString(dlpszDevices);
		bRet = TRUE;
	}

	else if (0 == StringCompare("GetEgressDevices", dlpszSplit[0]))
	{
		DLPSTR dlpszDevices;
		UARCHLONG ualIndex;

		dlpszDevices = AudioRouterGetEgressDevices(lpServer->lpArgs->hAudioRouter);

		if (NULLPTR != dlpszDevices)
		{
			for (
				ualIndex = 0;
				FALSE == STRING_SPLIT_NULL_TERMINATOR(dlpszDevices[ualIndex]);
				ualIndex++
			){
				if (ASCII_NULL_TERMINATOR == dlpszDevices[ualIndex][0])
				{ continue; }

				StringConcatenateSafe(
					lpszResponse,
					dlpszDevices[ualIndex],
					ualResponseSize
				);

				StringConcatenateSafe(
					lpszResponse,
					"\n",
					ualResponseSize
				);
			}
		}

		DestroySplitString(dlpszDevices);
		bRet = TRUE;
	}

	else if (0 == StringCompare("GetIngressMulticastStreams", dlpszSplit[0]))
	{
		DLPSTR dlpszDevices;
		UARCHLONG ualIndex;

		dlpszDevices = AudioRouterGetIngressMulticastStreams(lpServer->lpArgs->hAudioRouter);

		if (NULLPTR != dlpszDevices)
		{
			for (
				ualIndex = 0;
				FALSE == STRING_SPLIT_NULL_TERMINATOR(dlpszDevices[ualIndex]);
				ualIndex++
			){
				if (ASCII_NULL_TERMINATOR == dlpszDevices[ualIndex][0])
				{ continue; }

				StringConcatenateSafe(
					lpszResponse,
					dlpszDevices[ualIndex],
					ualResponseSize
				);

				StringConcatenateSafe(
					lpszResponse,
					"\n",
					ualResponseSize
				);
			}
		}

		DestroySplitString(dlpszDevices);
		bRet = TRUE;

	}
	
	else if (0 == StringCompare("GetEgressMulticastStreams", dlpszSplit[0]))
	{
		DLPSTR dlpszDevices;
		UARCHLONG ualIndex;

		dlpszDevices = AudioRouterGetEgressMulticastStreams(lpServer->lpArgs->hAudioRouter);

		if (NULLPTR != dlpszDevices)
		{
			for (
				ualIndex = 0;
				FALSE == STRING_SPLIT_NULL_TERMINATOR(dlpszDevices[ualIndex]);
				ualIndex++
			){
				if (ASCII_NULL_TERMINATOR == dlpszDevices[ualIndex][0])
				{ continue; }

				StringConcatenateSafe(
					lpszResponse,
					dlpszDevices[ualIndex],
					ualResponseSize
				);

				StringConcatenateSafe(
					lpszResponse,
					"\n",
					ualResponseSize
				);
			}
		}
		
		DestroySplitString(dlpszDevices);
		bRet = TRUE;

	}

	else if (0 == StringCompare("Quit", dlpszSplit[0]))
	{ bRet = FALSE; }

	DestroySplitString(dlpszSplit);
	return bRet;
}




INTERNAL_OPERATION
LPVOID
__ClientConnect(
	_In_Opt_ 	LPVOID 			lpParam
){
	LPCLIENT_PARAM lpcpClient;
	IPV4_ADDRESS ip4Client;
	CSTRING csAddress[0x40];
	CSTRING csBuffer[0x400];
	
	lpcpClient = (LPCLIENT_PARAM)lpParam;

	INFINITE_LOOP()
	{
		if (0 != SocketBytesInQueue4(lpcpClient->hClient))
		{
			CSTRING csBuffer[4096];
			CSTRING csResponse[4096];
			UARCHLONG ualSpot;

			ZeroMemory(csBuffer, sizeof(csBuffer));
			ZeroMemory(csResponse, sizeof(csResponse));
			lpcpClient->ulTimeout = 0;

			SocketReceive4(lpcpClient->hClient, csBuffer, sizeof(csBuffer) - 1);

			ualSpot = StringLength(csBuffer) - 1;
			if ('\n' == csBuffer[ualSpot])
			{ csBuffer[ualSpot] = 0; }
			
			if (ASCII_NULL_TERMINATOR == csBuffer[0])
			{ continue; }

			if (FALSE == __HandleCommand(lpcpClient->lpServer, csBuffer, csResponse, sizeof(csResponse) - 1))
			{
				DestroyObject(lpcpClient->hClient);
				FreeMemory(lpcpClient);
				DestroyObject(GetCurrentThread());
			}

			if (ASCII_NULL_TERMINATOR != csResponse[0])
			{ SocketSend4(lpcpClient->hClient, csResponse, StringLength(csResponse)); }
		}
		else
		{
			if (0x4000 == lpcpClient->ulTimeout)
			{ JUMP(__Finished); }
			lpcpClient->ulTimeout++;
			Sleep(5);
		}
	}


	__Finished:

	ZeroMemory(csAddress, sizeof(csAddress));
	ZeroMemory(csBuffer, sizeof(csBuffer));
	
	ip4Client = GetSocketIpAddress4(lpcpClient->hClient);
	ConvertIpv4AddressToString(ip4Client, csAddress, sizeof(csAddress));
	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer),
		"Client Disonnected At %s\n",
		csAddress
	);
	if (NULL_OBJECT != lpcpClient->lpServer->lpArgs->hLogFile)
	{ WriteLog(lpcpClient->lpServer->lpArgs->hLogFile, csBuffer, LOG_INFO, 0); }
	WriteFile(GetStandardOut(), csBuffer, 0);		

	DestroyObject(lpcpClient->hClient);
	FreeMemory(lpcpClient);
	DestroyObject(lpcpClient->hThread);
	return NULLPTR;
}




INTERNAL_OPERATION
LPVOID
__ServerTick(
	_In_Opt_ 	LPVOID			lpParam
){
	if (NULLPTR == lpParam)
	{ PostQuitMessage(1); }

	HANDLE hClient;
	LPSERVER lpServer;
	LPCLIENT_PARAM lpcpClient;
	IPV4_ADDRESS ip4Client;
	CSTRING csAddress[0x40];
	CSTRING csBuffer[0x400];

	ZeroMemory(csAddress, sizeof(csAddress));
	ZeroMemory(csBuffer, sizeof(csBuffer));

	lpServer = (LPSERVER)lpParam;
	
	WaitForSingleObject(lpServer->hLock, INFINITE_WAIT_TIME);
	ReleaseSingleObject(lpServer->hLock);

	INFINITE_LOOP()
	{
		hClient = AcceptConnection4(lpServer->hSocket);
		ip4Client = GetSocketIpAddress4(hClient);
		ConvertIpv4AddressToString(ip4Client, csAddress, sizeof(csAddress));
		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer),
			"Client Connected At %s\n",
			csAddress
		);

		if (NULL_OBJECT != lpServer->lpArgs->hLogFile)
		{ WriteLog(lpServer->lpArgs->hLogFile, csBuffer, LOG_INFO, 0); }

		WriteFile(GetStandardOut(), csBuffer, 0);		

		lpcpClient = GlobalAllocAndZero(sizeof(CLIENT_PARAM));
		if (NULLPTR == lpcpClient)
		{ PostQuitMessage(1); }

		lpcpClient->hClient = hClient;
		lpcpClient->lpServer = lpServer;
		lpcpClient->ulTimeout = 0;

		lpcpClient->hThread = CreateThread(__ClientConnect, (LPVOID)lpcpClient, NULLPTR);
	}

	return NULLPTR;
}




_Success_(return != FALSE, _Non_Locking_)
LPSERVER
SpawnServer(
	_In_ 		LPARGUMENTS		lpArgs
){
	EXIT_IF_UNLIKELY_NULL(lpArgs, NULLPTR);

	LPSERVER lpServer;

	lpServer = GlobalAllocAndZero(sizeof(SERVER));
	EXIT_IF_UNLIKELY_NULL(lpArgs, NULLPTR);

	/* Set Up SERVER */
	lpServer->lpArgs = lpArgs;
	lpServer->hSocket = CreateSocketWithAddress4(lpArgs->usBindPort, lpArgs->ip4Bind);
	EXIT_IF_UNLIKELY_NULL(lpServer->hSocket, NULLPTR);

	/* Create The Lock */
	lpServer->hLock = CreateCriticalSection();
	EXIT_IF_UNLIKELY_NULL(lpServer->hLock, NULLPTR);

	/* Acquire Lock */
	WaitForSingleObject(lpServer->hLock, INFINITE_WAIT_TIME);

	/* Bind On Socket */
	EXIT_IF_VALUE(BindOnSocket4(lpServer->hSocket), FALSE, NULLPTR);
	EXIT_IF_VALUE(ListenOnBoundSocket4(lpServer->hSocket, 0x400), FALSE, NULLPTR);

	/* Spawn Tick Thread */
	lpServer->hServerThread = CreateThread(__ServerTick, (LPVOID)lpServer, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpServer->hServerThread, NULLPTR);


	ReleaseSingleObject(lpServer->hLock);
	return lpServer;
}





#endif