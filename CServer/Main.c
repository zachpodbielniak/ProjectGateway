#include <PodNet/Ascii.h>
#include <PodNet/CLog/CLog.h>
#include <PodNet/CString/CString.h>
#include "../CAudioRouter/CAudioRouter.h"

#include "Args.c"
#include "Server.c"




LONG 
Main(
	_In_ 		LONG		lArgCount, 
	_In_Z_ 		DLPSTR		dlpszArgValues
){
	ARGUMENTS argParams;

	ZeroMemory(&argParams, sizeof(ARGUMENTS));

	lArgCount--;
	argParams.lArgCount = lArgCount;
	argParams.dlpszArgValues = dlpszArgValues;

	/* Set Defaults */
	argParams.sfFormat = PA_SAMPLE_S16LE;
	argParams.ulRate = 44100;
	argParams.byChannels = 2;
	argParams.ip4Bind = CreateIpv4AddressFromString("127.0.0.1");
	argParams.usBindPort = 6969;

	argParams.hAudioRouter = CreateAudioRouter();
	EXIT_IF_UNLIKELY_NULL(argParams.hAudioRouter, 1);

	EXIT_IF_VALUE(ReadInArguments(&argParams), FALSE, 1);

	SpawnServer(&argParams);

	Sleep(1000);

	CSTRING csBuffer[4096];
	AudioRouterGetStatus(argParams.hAudioRouter, csBuffer, sizeof(csBuffer));

	PrintFormat("%s", csBuffer);

	WaitForSingleObject(argParams.hAudioRouter, INFINITE_WAIT_TIME);
	return 0;
}