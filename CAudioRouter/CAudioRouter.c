#include "CAudioRouter.h"


#define HANDLE_TYPE_AUDIO_ROUTER 		0xDEAF
#define AUDIO_ROUTER_DEVICE_STRING_SIZE		0x400
#define AUDIO_ROUTER_BUFFER_SIZE		1456
#define EGRESS_NODE_MAX_SUPPORTED		0x32
#define EGRESS_MULTICAST_MAX_SUPPORTED		0x32
#define EGRESS_UNICAST_MAX_SUPPORTED		0x32


#define MULTICAST_LISTEN_IPV4			"239.255.175.222"


typedef struct __AUDIO_MULTICAST
{
	HANDLE		hAudioRouter;
	HANDLE		hSocket;
	IPV4_ADDRESS	ip4Address;
	IPV6_ADDRESS 	ip6Address;
	USHORT		usPort;
	BOOL 		bIsIpv6;
} AUDIO_MULTICAST, *LPAUDIO_MULTICAST;




typedef struct __AUDIO_UNICAST
{
	HANDLE		hAudioRouter;
	HANDLE 		hSocket;
	IPV4_ADDRESS	ip4Address;
	IPV6_ADDRESS 	ip6Address;
	USHORT 		usPort;
	BOOL 		bIsIpv6;
} AUDIO_UNICAST, *LPAUDIO_UNICAST;




typedef struct __AUDIO_INGRESS_NODE
{
	HANDLE		hAudioRouter;
	LPPULSE 	lppStream;
	PULSE_SAMPLE	psSample;
	UARCHLONG 	ualIndex;
	LONG 		lError;
	LPSTR 		lpszDevice;
	BOOL 		bIsPaused;
} AUDIO_INGRESS_NODE, *LPAUDIO_INGRESS_NODE;




typedef struct __AUDIO_EGRESS_NODE
{
	HANDLE		hAudioRouter;
	LPPULSE 	lppStream;
	PULSE_SAMPLE	psSample;
	UARCHLONG 	ualIndex;
	LONG 		lError;
	LPSTR 		lpszDevice;
	BOOL 		bIsPaused;
} AUDIO_EGRESS_NODE, *LPAUDIO_EGRESS_NODE, **DLPAUDIO_EGRESS_NODE;




typedef struct __AUDIO_ROUTER
{
	INHERITS_FROM_HANDLE();
	HANDLE			hLock;
	HANDLE			hThread;
	HLINKEDLIST		hllIngressUnicast;
	HLINKEDLIST		hllEgressUnicast;
	HLINKEDLIST		hllIngressMulticast;
	HLINKEDLIST		hllEgressMulticast;
	HLINKEDLIST 		hllIngressNodes;
	HLINKEDLIST		hllEgressNodes;
	UARCHLONG 		ualIngressIndex;
	UARCHLONG 		ualEgressIndex;
	UARCHLONG 		ualIngressCount;
	UARCHLONG 		ualEgressCount;
	UARCHLONG 		ualIngressMulticastCount;
	UARCHLONG 		ualEgressMulticastCount;
	UARCHLONG		ualIngressUnicastCount;
	UARCHLONG 		ualEgressUnicastCount;
	CSTRING 		csStreamName[0x40];
	BOOL			bIsPaused;
	BOOL 			bIsRunning;
} AUDIO_ROUTER, *LPAUDIO_ROUTER;




INTERNAL_OPERATION
BOOL
__DestroyAudioRouter(
	_In_ 		HDERIVATIVE	hDerivative,
	_In_Opt_ 	ULONG		ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPAUDIO_ROUTER lparRouter;
	lparRouter = (LPAUDIO_ROUTER)hDerivative;

	/* Stop The Tick Thread */
	InterlockedCompareExchange(&(lparRouter->bIsRunning), FALSE, TRUE);
	WaitForSingleObject(lparRouter->hThread, INFINITE_WAIT_TIME);

	/* Destroy Open HANDLEs */
	DestroyObject(lparRouter->hLock);
	DestroyObject(lparRouter->hThread);

	/* Destroy Linked Lists */
	if (NULLPTR != lparRouter->hllEgressNodes)
	{ DestroyLinkedList(lparRouter->hllEgressNodes); }

	if (NULLPTR != lparRouter->hllIngressNodes)
	{ DestroyLinkedList(lparRouter->hllIngressNodes); }


	/* If Using Multicast, Destroy */
	if (NULLPTR != lparRouter->hllEgressMulticast)
	{ DestroyLinkedList(lparRouter->hllEgressMulticast); }
	
	if (NULLPTR != lparRouter->hllIngressMulticast)
	{ DestroyLinkedList(lparRouter->hllIngressMulticast); }
	
	
	/* If Using Uniicast, Destroy */
	if (NULLPTR != lparRouter->hllEgressUnicast)
	{ DestroyLinkedList(lparRouter->hllEgressUnicast); }
	
	if (NULLPTR != lparRouter->hllIngressUnicast)
	{ DestroyLinkedList(lparRouter->hllIngressUnicast); }


	return TRUE;
}




INTERNAL_OPERATION
BOOL
__WaitForAudioRouter(
	_In_ 		HANDLE		hAudioRouter,
	_In_ 		ULONGLONG	ullMilliSeconds
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	return WaitForSingleObject(lparRouter->hThread, ullMilliSeconds);
}




/* 
	This is the function that the AudioRouter will run
	to constantly read in data from a stream, and output
	to another stream.

	The way this algorithm is designed, is that is can
	be a one-to-one, one-to-many, many-to-one, or 
	many-to-many relation of Ingress to Egress
	devices.

	As this is a single-threaded operation, there will
	be a finite limit to the amount of devices supported,
	depending on hardware.

	TODO: Look at setting thread priority to high.
*/
INTERNAL_OPERATION
LPVOID
__AudioRouterTick(
	_In_Opt_ 	LPVOID		lpParam
){
	/* TODO: Add better error handling here! */
	EXIT_IF_UNLIKELY_NULL(lpParam, NULLPTR);

	LPAUDIO_ROUTER lparRouter;
	LPVOID lpTraversal;
	LPAUDIO_INGRESS_NODE lpinNode;
	LPAUDIO_EGRESS_NODE lpegNode;

	/* Double Pointer To Prevent Multiple List Traversals */
	LPAUDIO_EGRESS_NODE dlpegNodes[EGRESS_NODE_MAX_SUPPORTED];
	LPAUDIO_MULTICAST dlpamNodes[EGRESS_MULTICAST_MAX_SUPPORTED];
	LPAUDIO_UNICAST dlpauNodes[EGRESS_UNICAST_MAX_SUPPORTED];
	UARCHLONG ualIndex, ualCount, ualMulticastCount, ualUnicastCount, ualReceive;
	BYTE lpbyBuffer[AUDIO_ROUTER_BUFFER_SIZE];
	BOOL bPlayedSomething;

	lparRouter = (LPAUDIO_ROUTER)lpParam;
	ualMulticastCount = 0;
	ualUnicastCount = 0;

	while(FALSE != InterlockedAcquire(&(lparRouter->bIsRunning)))
	{
		if (FALSE != lparRouter->bIsPaused)
		{ 
			Sleep(5);
			continue;
		}

		bPlayedSomething = FALSE;

		/* We Reset On Every Tick */
		ZeroMemory(dlpegNodes, sizeof(dlpegNodes));
		ZeroMemory(dlpamNodes, sizeof(dlpamNodes));

		/* Grab The Lock */		
		WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);


		/* Get All Egress Nodes */
		lpTraversal = LinkedListTraverse(lparRouter->hllEgressNodes, NULLPTR);
		for (
			ualIndex = 0;
			NULLPTR != lpTraversal;
			ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllEgressNodes, lpTraversal)
		){
			if (NULLPTR != lpTraversal)
			{ dlpegNodes[ualIndex] = (LPAUDIO_EGRESS_NODE)LinkedListAtNode(lparRouter->hllEgressNodes, lpTraversal); }
		}

		ualCount = ualIndex + 0x01U;

		/* Get All Egress Multicast */
		if (NULLPTR != lparRouter->hllEgressMulticast)
		{
			lpTraversal = LinkedListTraverse(lparRouter->hllEgressMulticast, NULLPTR);
			for (
				ualIndex = 0;
				NULLPTR != lpTraversal;
				ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllEgressMulticast, lpTraversal)
			){
				if (NULLPTR != lpTraversal)
				{ dlpamNodes[ualIndex] = (LPAUDIO_MULTICAST)LinkedListAtNode(lparRouter->hllEgressMulticast, lpTraversal); }
			}
		
			ualMulticastCount = ualIndex + 0x01U;
		}

		/* Get All Egress Unicast */
		if (NULLPTR != lparRouter->hllEgressUnicast)
		{
			lpTraversal = LinkedListTraverse(lparRouter->hllEgressUnicast, NULLPTR);
			for (
				ualIndex = 0;
				NULLPTR != lpTraversal;
				ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllEgressUnicast, lpTraversal)
			){
				if (NULLPTR != lpTraversal)
				{ dlpauNodes[ualIndex] = (LPAUDIO_UNICAST)LinkedListAtNode(lparRouter->hllEgressUnicast, lpTraversal); }
			}

			ualUnicastCount = ualIndex + 0x01U;
		}

		/* Multicast Read */
		if (NULLPTR != lparRouter->hllIngressMulticast)
		{
			for (
				ualIndex = 0, lpTraversal = LinkedListTraverse(lparRouter->hllIngressMulticast, NULLPTR);
				NULLPTR != lpTraversal;
				ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllIngressMulticast, lpTraversal)
			){
				LPAUDIO_MULTICAST lpamNode;
				lpamNode = (LPAUDIO_MULTICAST)LinkedListAtNode(lparRouter->hllIngressMulticast, lpTraversal);

				if (0 != SocketBytesInQueue4(lpamNode->hSocket))
				{		
					ZeroMemory(lpbyBuffer, sizeof(lpbyBuffer));
					
					ualReceive = SocketReceive4(
						lpamNode->hSocket,
						lpbyBuffer,
						AUDIO_ROUTER_BUFFER_SIZE
					);

					if (0 != ualReceive)
					{ bPlayedSomething = TRUE; }

					/* Write To Multicast */
					for (
						ualIndex = 0;
						ualIndex < ualMulticastCount;
						ualIndex++
					){
						/* TODO: Add Better Error Handling */
						if (NULLPTR == dlpamNodes[ualIndex])
						{ continue; }

						SocketSendTo4(
							dlpamNodes[ualIndex]->hSocket,
							lpbyBuffer,
							ualReceive
						);
					}

					/* Write To Unicast */
					for (
						ualIndex = 0;
						ualIndex < ualUnicastCount;
						ualIndex++
					){
						/* TODO: Add Better Error Handling */
						if (NULLPTR == dlpauNodes[ualIndex])
						{ continue; }
						
						SocketSendTo4(
							dlpamNodes[ualIndex]->hSocket,
							lpbyBuffer,
							ualReceive
						);
					}


					/* Write To Each Egress */
					for (
						ualIndex = 0;
						ualIndex < ualCount;
						ualIndex++
					){
						/* TODO: Add Better Error Handling */
						if (NULLPTR == dlpegNodes[ualIndex])
						{ continue; }

						if (0 > pa_simple_write(
							dlpegNodes[ualIndex]->lppStream,
							lpbyBuffer,
							ualReceive,
							&(dlpegNodes[ualIndex]->lError)
						)){ continue; }
					}

				}
			}
		}


		/* Unicast Read */
		if (NULLPTR != lparRouter->hllIngressUnicast)
		{
			for (
				ualIndex = 0, lpTraversal = LinkedListTraverse(lparRouter->hllIngressUnicast, NULLPTR);
				NULLPTR != lpTraversal;
				ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllIngressUnicast, lpTraversal)
			){
				LPAUDIO_UNICAST lpauNode;
				lpauNode = (LPAUDIO_UNICAST)LinkedListAtNode(lparRouter->hllIngressUnicast, lpTraversal);

				if (0 != SocketBytesInQueue4(lpauNode->hSocket))
				{		
					ZeroMemory(lpbyBuffer, sizeof(lpbyBuffer));
					
					ualReceive = SocketReceive4(
						lpauNode->hSocket,
						lpbyBuffer,
						AUDIO_ROUTER_BUFFER_SIZE
					);

					if (0 != ualReceive)
					{ bPlayedSomething = TRUE; }

					/* Write To Multicast */
					for (
						ualIndex = 0;
						ualIndex < ualMulticastCount;
						ualIndex++
					){
						/* TODO: Add Better Error Handling */
						if (NULLPTR == dlpamNodes[ualIndex])
						{ continue; }

						SocketSendTo4(
							dlpamNodes[ualIndex]->hSocket,
							lpbyBuffer,
							ualReceive
						);
					}


					/* Write To Unicast */
					for (
						ualIndex = 0;
						ualIndex < ualUnicastCount;
						ualIndex++
					){
						/* TODO: Add Better Error Handling */
						if (NULLPTR == dlpauNodes[ualIndex])
						{ continue; }
						
						SocketSendTo4(
							dlpamNodes[ualIndex]->hSocket,
							lpbyBuffer,
							ualReceive
						);
					}


					/* Write To Each Egress */
					for (
						ualIndex = 0;
						ualIndex < ualCount;
						ualIndex++
					){
						/* TODO: Add Better Error Handling */
						if (NULLPTR == dlpegNodes[ualIndex])
						{ continue; }

						if (0 > pa_simple_write(
							dlpegNodes[ualIndex]->lppStream,
							lpbyBuffer,
							ualReceive,
							&(dlpegNodes[ualIndex]->lError)
						)){ continue; }
					}

				}
			}
		}

		/* Traverse The Ingress */
		lpTraversal = NULLPTR;
		while (NULLPTR != (lpTraversal = LinkedListTraverse(lparRouter->hllIngressNodes, lpTraversal)))
		{
			if (NULLPTR == lpTraversal)
			{ JUMP(__Finished); }

			lpinNode = (LPAUDIO_INGRESS_NODE)LinkedListAtNode(lparRouter->hllIngressNodes, lpTraversal);
			if (NULLPTR == lpinNode)
			{ continue; }

			ZeroMemory(lpbyBuffer, sizeof(lpbyBuffer));
			/* TODO: Add Better Error Handling */
			if (0 > pa_simple_read(
				lpinNode->lppStream, 
				lpbyBuffer, 
				AUDIO_ROUTER_BUFFER_SIZE, 
				&(lpinNode->lError)
			)){ continue; }

			bPlayedSomething = TRUE;
			
			/* Write To Multicast */
			for (
				ualIndex = 0;
				ualIndex < ualMulticastCount;
				ualIndex++
			){
				/* TODO: Add Better Error Handling */
				if (NULLPTR == dlpamNodes[ualIndex])
				{ continue; }

				SocketSendTo4(
					dlpamNodes[ualIndex]->hSocket,
					lpbyBuffer,
					AUDIO_ROUTER_BUFFER_SIZE
				);
				
				bPlayedSomething = TRUE;
			}

			/* Write To Unicast */
			for (
				ualIndex = 0;
				ualIndex < ualUnicastCount;
				ualIndex++
			){
				/* TODO: Add Better Error Handling */
				if (NULLPTR == dlpauNodes[ualIndex])
				{ continue; }
				
				SocketSendTo4(
					dlpamNodes[ualIndex]->hSocket,
					lpbyBuffer,
					AUDIO_ROUTER_BUFFER_SIZE
				);

				bPlayedSomething = TRUE;
			}

			/* Write To Each Egress */
			for (
				ualIndex = 0;
				ualIndex < ualCount;
				ualIndex++
			){
				/* TODO: Add Better Error Handling */
				if (NULLPTR == dlpegNodes[ualIndex])
				{ continue; }

				if (NULLPTR == dlpegNodes[ualIndex]->lppStream)
				{ continue; }

				if (0 > pa_simple_write(
					dlpegNodes[ualIndex]->lppStream,
					lpbyBuffer,
					AUDIO_ROUTER_BUFFER_SIZE,
					&(dlpegNodes[ualIndex]->lError)
				)){ PrintFormat("%s\n", pa_strerror(dlpegNodes[ualIndex]->lError)); }

				bPlayedSomething = TRUE;
			}
		}


		ReleaseSingleObject(lparRouter->hLock);

		if (FALSE == bPlayedSomething)
		{ Sleep(5); }
	}


	/* Cleanup Code */
	__Finished:
	
	lpTraversal = LinkedListTraverse(lparRouter->hllIngressNodes, NULLPTR);
	for (
		lpinNode = NULLPTR;
		lpTraversal != NULLPTR;
		lpTraversal = LinkedListTraverse(lparRouter->hllIngressNodes, lpTraversal)
	){
		lpinNode = LinkedListAtNode(lparRouter->hllIngressNodes, lpTraversal);
		if (NULLPTR == lpinNode)
		{ continue; }

		pa_simple_drain(lpinNode->lppStream, NULLPTR);
		pa_simple_free(lpinNode->lppStream);
		
		FreeMemory(lpinNode->lpszDevice);
	}

	lpTraversal = LinkedListTraverse(lparRouter->hllEgressNodes, NULLPTR);
	for (
		lpegNode = NULLPTR;
		lpTraversal != NULLPTR;
		lpTraversal = LinkedListTraverse(lparRouter->hllEgressNodes, lpTraversal)
	){
		lpegNode = LinkedListAtNode(lparRouter->hllEgressNodes, lpTraversal);
		if (NULLPTR == lpegNode)
		{ continue; }

		pa_simple_drain(lpegNode->lppStream, NULLPTR);
		pa_simple_free(lpegNode->lppStream);
		
		FreeMemory(lpegNode->lpszDevice);
	}


	/* Free Ingress Multicast */ 
	if (NULLPTR != lparRouter->hllIngressMulticast)
	{
		lpTraversal = LinkedListTraverse(lparRouter->hllIngressMulticast, NULLPTR);
		for (
			ualIndex = 0;
			NULLPTR != lpTraversal;
			ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllIngressMulticast, lpTraversal)
		){
			LPAUDIO_MULTICAST lpamNode;
			if (NULLPTR != lpTraversal)
			{
				lpamNode = (LPAUDIO_MULTICAST)LinkedListAtNode(lparRouter->hllIngressMulticast, lpTraversal);
				DestroyObject(lpamNode->hSocket);
			}
		}
	}


	/* Free Egress Multicast */ 
	if (NULLPTR != lparRouter->hllEgressMulticast)
	{
		lpTraversal = LinkedListTraverse(lparRouter->hllEgressMulticast, NULLPTR);
		for (
			ualIndex = 0;
			NULLPTR != lpTraversal;
			ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllEgressMulticast, lpTraversal)
		){
			LPAUDIO_MULTICAST lpamNode;
			if (NULLPTR != lpTraversal)
			{
				lpamNode = (LPAUDIO_MULTICAST)LinkedListAtNode(lparRouter->hllEgressMulticast, lpTraversal);
				DestroyObject(lpamNode->hSocket);
			}
		}
	}



	return NULLPTR;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
GATEWAY_API
HANDLE
CreateAudioRouter(
	VOID
){
	/* EXIT_IF_UNLIKELY_NULL(lpcszIngress, NULL_OBJECT); */

	HANDLE hRouter;
	LPAUDIO_ROUTER lparRouter;

	lparRouter = GlobalAllocAndZero(sizeof(AUDIO_ROUTER));
	EXIT_IF_UNLIKELY_NULL(lparRouter, NULL_OBJECT);

	StringCopySafe(
		lparRouter->csStreamName, 
		"Project Gateway", 
		sizeof(lparRouter->csStreamName)
	);

	lparRouter->hLock = CreateSemaphore(1);

	lparRouter->hllEgressNodes = CreateLinkedList(sizeof(AUDIO_EGRESS_NODE), FALSE, NULL_OBJECT);
	lparRouter->hllIngressNodes = CreateLinkedList(sizeof(AUDIO_INGRESS_NODE), FALSE, NULL_OBJECT);

	lparRouter->ualEgressIndex = 0;
	lparRouter->ualIngressIndex = 0;
	lparRouter->bIsRunning = TRUE;
	lparRouter->bIsPaused = FALSE;

	hRouter = CreateHandleWithSingleInheritor(
		lparRouter,
		&(lparRouter->hThis),
		HANDLE_TYPE_AUDIO_ROUTER,
		__DestroyAudioRouter,
		0,
		NULLPTR,
		0,	
		NULLPTR,
		0,
		&(lparRouter->ullId),
		0	
	);

	lparRouter->hThread = CreateThread(__AudioRouterTick, (LPVOID)lparRouter, NULLPTR);
	SetHandleEventWaitFunction(hRouter, __WaitForAudioRouter, 0);

	return hRouter;
}




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddIngressDevice(
	_In_ 		HANDLE			hAudioRouter,
	_In_ 		LPCSTR RESTRICT		lpcszIngress,
	_In_		SAMPLE_FORMAT		sfFormat,
	_In_ 		ULONG			ulRate,
	_In_ 		BYTE			byChannels,
	_Out_Opt_ 	LPUARCHLONG		lpualIdentifier
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszIngress, FALSE);

	LPAUDIO_ROUTER lparRouter;
	AUDIO_INGRESS_NODE aunDevice;
	LPAUDIO_INGRESS_NODE lpaunDevice;
	UARCHLONG ualStringSize, ualIndex;
	DLPSTR dlpszDevices;

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);

	/* Check to make sure we don't have this device already */
	dlpszDevices = AudioRouterGetIngressDevices(hAudioRouter);

	if (NULLPTR != dlpszDevices)
	{
		for (
			ualIndex = 0;
			FALSE == STRING_SPLIT_NULL_TERMINATOR(dlpszDevices[ualIndex]);
			ualIndex++
		){
			if (ASCII_NULL_TERMINATOR == dlpszDevices[ualIndex][0])
			{ continue; }

			if (0 == StringCompare(lpcszIngress, dlpszDevices[ualIndex]))
			{ 
				DestroySplitString(dlpszDevices);
				return FALSE; 
			}
		}
	}

	DestroySplitString(dlpszDevices);

	/* Basic Configuration */
	aunDevice.bIsPaused = FALSE;
	aunDevice.hAudioRouter = hAudioRouter;
	aunDevice.psSample.channels = byChannels;
	aunDevice.psSample.format= sfFormat;
	aunDevice.psSample.rate = ulRate;
	aunDevice.ualIndex = (lparRouter->ualIngressIndex)++;

	/* Save A Copy Of Device Name String */
	ualStringSize = StringLength(lpcszIngress);
	aunDevice.lpszDevice = GlobalAllocAndZero(ualStringSize + 0x01U);
	CopyMemory(aunDevice.lpszDevice, lpcszIngress, ualStringSize);

	/* Acquire Lock On The Router */
	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	lparRouter->ualIngressCount++;

	/* Attach Our Node And Use For Proper Addresses */
	LinkedListAttachBack(lparRouter->hllIngressNodes, &aunDevice);
	lpaunDevice = LinkedListBack(lparRouter->hllIngressNodes);

	lpaunDevice->lppStream = pa_simple_new(
		NULLPTR,
		"Project Gateway",
		PA_STREAM_RECORD,
		lpaunDevice->lpszDevice,
		lparRouter->csStreamName,
		&(lpaunDevice->psSample),
		NULLPTR,
		NULLPTR,
		&(lpaunDevice->lError)
	);

	/* Release The Lock */
	ReleaseSingleObject(lparRouter->hLock);

	if (NULLPTR != lpualIdentifier)
	{ *lpualIdentifier = aunDevice.ualIndex; }

	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterRemoveIngressDevice(
	_In_ 		HANDLE			hAudioRouter,
	_In_ 		LPCSTR RESTRICT		lpcszIngress
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	LPVOID lpTraversal;
	LPAUDIO_INGRESS_NODE lpinNode;

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);
	
	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	lparRouter->ualIngressCount--;

	lpTraversal = LinkedListTraverse(lparRouter->hllIngressNodes, NULLPTR);
	for (
		lpinNode = NULLPTR;
		lpTraversal != NULLPTR;
		lpTraversal = LinkedListTraverse(lparRouter->hllIngressNodes, lpTraversal)
	){
		lpinNode = LinkedListAtNode(lparRouter->hllIngressNodes, lpTraversal);
		if (0 == StringCompare(lpcszIngress, lpinNode->lpszDevice))
		{
			LinkedListRemoveAtNode(lparRouter->hllIngressNodes, lpTraversal, FALSE);
			
			pa_simple_drain(lpinNode->lppStream, NULLPTR);
			pa_simple_free(lpinNode->lppStream);
		
			FreeMemory(lpinNode->lpszDevice);
			FreeMemory(lpinNode);
			
			ReleaseSingleObject(lparRouter->hLock);
			return TRUE;
		}
	}

	ReleaseSingleObject(lparRouter->hLock);
	return FALSE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddEgressDevice(
	_In_ 		HANDLE			hAudioRouter,
	_In_ 		LPCSTR RESTRICT		lpcszEgress,
	_In_ 		SAMPLE_FORMAT		sfFormat,
	_In_ 		ULONG 			ulRate,
	_In_		BYTE 			byChannels,
	_Out_Opt_ 	LPUARCHLONG 		lpualIdentifier
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszEgress, FALSE);

	LPAUDIO_ROUTER lparRouter;
	AUDIO_EGRESS_NODE aegDevice;
	LPAUDIO_EGRESS_NODE lpaegDevice;
	UARCHLONG ualStringSize, ualIndex;
	DLPSTR dlpszDevices;

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);

	/* Check to make sure we don't have this device already */
	dlpszDevices = AudioRouterGetEgressDevices(hAudioRouter);

	if (NULLPTR != dlpszDevices)
	{
		for (
			ualIndex = 0;
			FALSE == STRING_SPLIT_NULL_TERMINATOR(dlpszDevices[ualIndex]);
			ualIndex++
		){
			if (ASCII_NULL_TERMINATOR == dlpszDevices[ualIndex][0])
			{ continue; }

			if (0 == StringCompare(lpcszEgress, dlpszDevices[ualIndex]))
			{ 
				DestroySplitString(dlpszDevices);
				return FALSE; 
			}
		}
	}

	DestroySplitString(dlpszDevices);

	/* Basic Configuration */
	aegDevice.bIsPaused = FALSE;
	aegDevice.hAudioRouter = hAudioRouter;
	aegDevice.psSample.channels = byChannels;
	aegDevice.psSample.format= sfFormat;
	aegDevice.psSample.rate = ulRate;
	aegDevice.ualIndex = (lparRouter->ualEgressIndex)++;

	/* Save A Copy Of Device Name String */
	ualStringSize = StringLength(lpcszEgress);
	aegDevice.lpszDevice = GlobalAllocAndZero(ualStringSize + 0x01U);
	CopyMemory(aegDevice.lpszDevice, lpcszEgress, ualStringSize);

	/* Acquire Lock On The Router */
	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);
	
	lparRouter->ualEgressCount++;

	/* Attach Our Node And Use For Proper Addresses */
	LinkedListAttachBack(lparRouter->hllEgressNodes, &aegDevice);
	lpaegDevice = LinkedListBack(lparRouter->hllEgressNodes);

	/* Create A Play Back Device */
	lpaegDevice->lppStream = pa_simple_new(
		NULLPTR,
		"Project Gateway",
		PA_STREAM_PLAYBACK,
		lpaegDevice->lpszDevice,
		lparRouter->csStreamName,
		&(lpaegDevice->psSample),
		NULLPTR,
		NULLPTR,
		&(lpaegDevice->lError)
	);

	/* Release The Lock */
	ReleaseSingleObject(lparRouter->hLock);

	if (NULLPTR != lpualIdentifier)
	{ *lpualIdentifier = aegDevice.ualIndex; }

	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterRemoveEgressDevice(
	_In_ 		HANDLE			hAudioRouter,
	_In_ 		LPCSTR RESTRICT		lpcszEgress
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	LPVOID lpTraversal;
	LPAUDIO_EGRESS_NODE lpegNode;

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	lparRouter->ualEgressCount--;

	/* Traverse The List */
	lpTraversal = LinkedListTraverse(lparRouter->hllEgressNodes, NULLPTR);
	for (
		lpegNode = NULLPTR;
		lpTraversal != NULLPTR;
		lpTraversal = LinkedListTraverse(lparRouter->hllEgressNodes, lpTraversal)
	){
		lpegNode = LinkedListAtNode(lparRouter->hllEgressNodes, lpTraversal);
		
		/* Compare By String */
		if (0 == StringCompare(lpcszEgress, lpegNode->lpszDevice))
		{
			/* When Found, Just Destroy All Associated Data */
			LinkedListRemoveAtNode(lparRouter->hllEgressNodes, lpTraversal, FALSE);
			
			pa_simple_drain(lpegNode->lppStream, NULLPTR);
			pa_simple_free(lpegNode->lppStream);
		
			FreeMemory(lpegNode->lpszDevice);
			FreeMemory(lpegNode);
			
			ReleaseSingleObject(lparRouter->hLock);
			return TRUE;
		}
	}

	ReleaseSingleObject(lparRouter->hLock);
	return FALSE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddIngressMulticastRouting(
	_In_ 		HANDLE 		hAudioRouter,
	_In_Z_ 		LPCSTR 		lpcszGroupAddress,
	_In_Z_ 		LPCSTR 		lpcszBindAddress,
	_In_ 		USHORT 		usPort
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	AUDIO_MULTICAST amIngress;
	IPV4_ADDRESS ip4Group, ip4Bind;
	BOOL bRet;
	
	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter); 
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);

	ip4Bind = CreateIpv4AddressFromString(lpcszBindAddress);
	ip4Group = CreateIpv4AddressFromString(lpcszGroupAddress);
	EXIT_IF_UNLIKELY_VALUE(IPV4_ADDRESS_IS_CLASS_D(ip4Group), FALSE, FALSE);

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	lparRouter->ualIngressMulticastCount++;

	/* Define All Multicast Params */
	amIngress.bIsIpv6 = FALSE;
	amIngress.hAudioRouter = hAudioRouter;
	amIngress.ip4Address = ip4Group;
	amIngress.hSocket = CreateUdpSocket4(usPort);
	amIngress.usPort = usPort;

	/* Bind */
	BindOnSocket4(amIngress.hSocket);

	/* Join The Multicast Group */
	bRet = JoinMulticastGroup4(
		amIngress.hSocket,
		ip4Group,
		ip4Bind
	);

	/* Create Linked List If Null */
	if (NULLPTR == lparRouter->hllIngressMulticast)
	{
		lparRouter->hllIngressMulticast = CreateLinkedList(sizeof(AUDIO_MULTICAST), FALSE, NULL_OBJECT);
		/* TODO: Add Error Handling */
	}

	/* Attach And Release */
	LinkedListAttachBack(lparRouter->hllIngressMulticast, &amIngress);
	ReleaseSingleObject(lparRouter->hLock);
	
	return bRet;	
}




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddEgressMulticastRouting(
	_In_ 		HANDLE			hAudioRouter,
	_In_Z_ 		LPCSTR			lpcszGroupAddress,
	_In_ 		USHORT			usPort
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	AUDIO_MULTICAST amEgress;

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter); 
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	lparRouter->ualEgressMulticastCount++;

	/* Define All Multicast Params */
	amEgress.bIsIpv6 = FALSE;
	amEgress.hAudioRouter = hAudioRouter;
	amEgress.ip4Address = CreateIpv4AddressFromString(lpcszGroupAddress);
	amEgress.hSocket = CreateUdpSocketWithAddress4(
		usPort,
		amEgress.ip4Address
	);
	amEgress.usPort = usPort;

	/* Create Linked List If Null */
	if (NULLPTR == lparRouter->hllEgressMulticast)
	{
		lparRouter->hllEgressMulticast = CreateLinkedList(sizeof(AUDIO_MULTICAST), FALSE, NULL_OBJECT);
		/* TODO: Added Error Handling */
	}

	/* Attach And Release */
	LinkedListAttachBack(lparRouter->hllEgressMulticast, &amEgress);
	ReleaseSingleObject(lparRouter->hLock);

	return TRUE;	
}




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddIngressUnicastDevice(
	_In_ 		HANDLE		hAudioRouter,
	_In_Z_ 		LPCSTR		lpcszUnicastAddress,
	_In_ 		USHORT 		usPort
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszUnicastAddress, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	AUDIO_UNICAST auNode;

	lparRouter = OBJECT_CAST(hAudioRouter, LPAUDIO_ROUTER);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	auNode.bIsIpv6 = FALSE;
	auNode.hAudioRouter = hAudioRouter;
	auNode.hSocket = CreateUdpSocket4(usPort);
	auNode.ip4Address = CreateIpv4AddressFromString(lpcszUnicastAddress);
	auNode.usPort = usPort;

	if (NULLPTR == lparRouter->hllIngressUnicast)
	{ 
		lparRouter->hllIngressUnicast = CreateLinkedList(sizeof(AUDIO_UNICAST), FALSE, FALSE);
		EXIT_IF_UNLIKELY_NULL(lparRouter->hllIngressUnicast, FALSE);
	}	

	/* Attach And Release */
	LinkedListAttachBack(lparRouter->hllIngressUnicast, &auNode);
	ReleaseSingleObject(lparRouter->hLock);

	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddEgressUnicastDevice(
	_In_ 		HANDLE		hAudioRouter,
	_In_Z_ 		LPCSTR		lpcszUnicastAddress,
	_In_ 		USHORT 		usPort
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszUnicastAddress, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	AUDIO_UNICAST auNode;

	lparRouter = OBJECT_CAST(hAudioRouter, LPAUDIO_ROUTER);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	auNode.bIsIpv6 = FALSE;
	auNode.hAudioRouter = hAudioRouter;
	auNode.ip4Address = CreateIpv4AddressFromString(lpcszUnicastAddress);
	auNode.hSocket = CreateUdpSocketWithAddress4(
		usPort,
		auNode.ip4Address
	);
	auNode.usPort = usPort;

	if (NULLPTR == lparRouter->hllIngressUnicast)
	{ 
		lparRouter->hllIngressUnicast = CreateLinkedList(sizeof(AUDIO_UNICAST), FALSE, FALSE);
		EXIT_IF_UNLIKELY_NULL(lparRouter->hllIngressUnicast, FALSE);
	}	

	/* Attach And Release */
	LinkedListAttachBack(lparRouter->hllIngressUnicast, &auNode);
	ReleaseSingleObject(lparRouter->hLock);

	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterPause(
	_In_ 		HANDLE		hAudioRouter
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);

	return InterlockedCompareExchangeHappened(&(lparRouter->bIsPaused), FALSE, TRUE);
}




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterResume(
	_In_ 		HANDLE		hAudioRouter
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);

	return InterlockedCompareExchangeHappened(&(lparRouter->bIsPaused), TRUE, FALSE);
}




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterGetStatus(
	_In_ 		HANDLE 		hAudioRouter,
	_Out_Z_		LPSTR 		lpszOutBuffer,
	_In_ 		UARCHLONG 	ualBufferSize
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpszOutBuffer, FALSE);
	EXIT_IF_UNLIKELY_NULL(ualBufferSize, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	CSTRING csBuffer[4096];	
	UARCHLONG ualSize;

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"Paused: %s\n"	\
		"Ingress Devices: %lu\n" \
		"Egress Devices: %lu\n" \
		"Ingress Multicast Streams: %lu\n" \
		"Egress Multicast Streams: %lu\n",
		(FALSE == lparRouter->bIsPaused) ? "FALSE" : "TRUE",
		lparRouter->ualIngressCount,
		lparRouter->ualEgressCount,
		lparRouter->ualIngressMulticastCount,
		lparRouter->ualEgressMulticastCount
	);

	ReleaseSingleObject(lparRouter->hLock);

	ualSize = (StringLength(csBuffer) <= ualBufferSize) ? StringLength(csBuffer) : ualBufferSize;
	CopyMemory(lpszOutBuffer, csBuffer, ualSize);
	
	return TRUE;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetIngressDevices(
	_In_ 		HANDLE 		hAudioRouter
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	DLPSTR dlpszSplit;
	UARCHLONG ualIndex;
	LPVOID lpTraversal;
	LPAUDIO_INGRESS_NODE lpinNode;
	CSTRING csBuffer[0x1000];

	ZeroMemory(csBuffer, sizeof(csBuffer));

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);
	dlpszSplit = NULLPTR;

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	lpinNode = NULLPTR;
	if (0 != lparRouter->ualIngressCount)
	{
		lpTraversal = LinkedListTraverse(lparRouter->hllIngressNodes, NULLPTR);
		for (
			ualIndex = 0;
			ualIndex < lparRouter->ualIngressCount;
			ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllIngressNodes, lpTraversal)
		){
			if (lpTraversal != NULLPTR)
			{ lpinNode = (LPAUDIO_INGRESS_NODE)LinkedListAtNode(lparRouter->hllIngressNodes, lpTraversal); }
			
			if (NULLPTR == lpinNode)
			{ continue; }

			StringConcatenateSafe(
				csBuffer,
				lpinNode->lpszDevice,
				sizeof(csBuffer) - 1
			);

			StringConcatenateSafe(
				csBuffer,
				" ",
				sizeof(csBuffer) - 1
			);

		}

		StringSplit(
			csBuffer,
			" ",
			&dlpszSplit
		);
	}

	ReleaseSingleObject(lparRouter->hLock);

	return dlpszSplit;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetEgressDevices(
	_In_ 		HANDLE 		hAudioRouter
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	DLPSTR dlpszSplit;
	UARCHLONG ualIndex;
	LPVOID lpTraversal;
	LPAUDIO_EGRESS_NODE lpegNode;
	CSTRING csBuffer[0x1000];

	ZeroMemory(csBuffer, sizeof(csBuffer));

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);
	dlpszSplit = NULLPTR;

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	lpegNode = NULLPTR;
	if (0 != lparRouter->ualEgressCount)
	{
		lpTraversal = LinkedListTraverse(lparRouter->hllEgressNodes, NULLPTR);
		for (
			ualIndex = 0;
			ualIndex < lparRouter->ualEgressCount;
			ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllEgressNodes, lpTraversal)
		){
			if (NULLPTR != lpTraversal)
			{ lpegNode = (LPAUDIO_EGRESS_NODE)LinkedListAtNode(lparRouter->hllEgressNodes, lpTraversal); }
			
			if (NULLPTR == lpegNode)
			{ continue; }

			StringConcatenateSafe(
				csBuffer,
				lpegNode->lpszDevice,
				sizeof(csBuffer) - 1
			);

			StringConcatenateSafe(
				csBuffer,
				" ",
				sizeof(csBuffer) - 1
			);

		}

		StringSplit(
			csBuffer,
			" ",
			&dlpszSplit
		);
	}


	ReleaseSingleObject(lparRouter->hLock);

	return dlpszSplit;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetIngressMulticastStreams(
	_In_ 		HANDLE 		hAudioRouter
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	DLPSTR dlpszSplit;
	UARCHLONG ualIndex;
	LPVOID lpTraversal;
	LPAUDIO_MULTICAST lpamNode;
	CSTRING csBuffer[0x1000];
	CSTRING csTemp[0x1000];
	CSTRING csAddress[0x100];

	ZeroMemory(csBuffer, sizeof(csBuffer));
	ZeroMemory(csTemp, sizeof(csTemp));
	ZeroMemory(csAddress, sizeof(csAddress));

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);
	dlpszSplit = NULLPTR;

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	lpamNode = NULLPTR;
	if (0 != lparRouter->ualIngressMulticastCount)
	{
		lpTraversal = LinkedListTraverse(lparRouter->hllIngressMulticast, NULLPTR);
		for (
			ualIndex = 0;
			ualIndex < lparRouter->ualIngressMulticastCount;
			ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllIngressMulticast, lpTraversal)
		){
			if (NULLPTR != lpTraversal)
			{ lpamNode = (LPAUDIO_MULTICAST)LinkedListAtNode(lparRouter->hllIngressMulticast, lpTraversal); }
			
			if (NULLPTR == lpamNode)
			{ continue; }
			
			ZeroMemory(csTemp, sizeof(csTemp));
			ZeroMemory(csAddress, sizeof(csAddress));

			ConvertIpv4AddressToString(
				lpamNode->ip4Address,
				csAddress,
				sizeof(csAddress) - 1
			);

			StringPrintFormatSafe(
				csTemp,
				sizeof(csTemp),
				"%s:%hu",
				csAddress,
				lpamNode->usPort
			);

			if (ualIndex != lparRouter->ualIngressMulticastCount - 1)
			{ StringConcatenateSafe(csTemp, " ", sizeof(csTemp) - 1); }

			StringConcatenateSafe(
				csBuffer,
				csTemp,
				sizeof(csBuffer) - 1
			);

		}

		StringSplit(
			csBuffer,
			" ",
			&dlpszSplit
		);
	}


	ReleaseSingleObject(lparRouter->hLock);

	return dlpszSplit;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetEgressMulticastStreams(
	_In_ 		HANDLE 		hAudioRouter
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	DLPSTR dlpszSplit;
	UARCHLONG ualIndex;
	LPVOID lpTraversal;
	LPAUDIO_MULTICAST lpamNode;
	CSTRING csBuffer[0x1000];
	CSTRING csTemp[0x1000];
	CSTRING csAddress[0x100];

	ZeroMemory(csBuffer, sizeof(csBuffer));
	ZeroMemory(csTemp, sizeof(csTemp));
	ZeroMemory(csAddress, sizeof(csAddress));

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);
	dlpszSplit = NULLPTR;

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	lpamNode = NULLPTR;
	if (0 != lparRouter->ualEgressMulticastCount)
	{
		lpTraversal = LinkedListTraverse(lparRouter->hllEgressMulticast, NULLPTR);
		for (
			ualIndex = 0;
			ualIndex < lparRouter->ualEgressMulticastCount;
			ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllEgressMulticast, lpTraversal)
		){
			if (NULLPTR != lpTraversal)
			{ lpamNode = (LPAUDIO_MULTICAST)LinkedListAtNode(lparRouter->hllEgressMulticast, lpTraversal); }
			
			if (NULLPTR == lpamNode)
			{ continue; }
			
			ZeroMemory(csTemp, sizeof(csTemp));
			ZeroMemory(csAddress, sizeof(csAddress));

			ConvertIpv4AddressToString(
				lpamNode->ip4Address,
				csAddress,
				sizeof(csAddress)
			);

			StringPrintFormatSafe(
				csTemp,
				sizeof(csTemp),
				"%s:%hu",
				csAddress,
				lpamNode->usPort
			);

			if (ualIndex != lparRouter->ualEgressMulticastCount - 1)
			{ StringConcatenateSafe(csTemp, " ", sizeof(csTemp)); }

			StringConcatenateSafe(
				csBuffer,
				csTemp,
				sizeof(csBuffer) - 1
			);

		}

		StringSplit(
			csBuffer,
			" ",
			&dlpszSplit
		);
	}


	ReleaseSingleObject(lparRouter->hLock);

	return dlpszSplit;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetIngressUnicastStreams(
	_In_ 		HANDLE 		hAudioRouter
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	DLPSTR dlpszSplit;
	UARCHLONG ualIndex;
	LPVOID lpTraversal;
	LPAUDIO_UNICAST lpauNode;
	CSTRING csBuffer[0x1000];
	CSTRING csTemp[0x1000];
	CSTRING csAddress[0x100];

	ZeroMemory(csBuffer, sizeof(csBuffer));
	ZeroMemory(csTemp, sizeof(csTemp));
	ZeroMemory(csAddress, sizeof(csAddress));

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);
	dlpszSplit = NULLPTR;

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	lpauNode = NULLPTR;
	if (0 != lparRouter->ualEgressUnicastCount)
	{
		lpTraversal = LinkedListTraverse(lparRouter->hllEgressUnicast, NULLPTR);
		for (
			ualIndex = 0;
			ualIndex < lparRouter->ualEgressUnicastCount;
			ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllEgressUnicast, lpTraversal)
		){
			if (NULLPTR != lpTraversal)
			{ lpauNode = (LPAUDIO_UNICAST)LinkedListAtNode(lparRouter->hllEgressUnicast, lpTraversal); }
			
			if (NULLPTR == lpauNode)
			{ continue; }
			
			ZeroMemory(csTemp, sizeof(csTemp));
			ZeroMemory(csAddress, sizeof(csAddress));

			ConvertIpv4AddressToString(
				lpauNode->ip4Address,
				csAddress,
				sizeof(csAddress)
			);

			StringPrintFormatSafe(
				csTemp,
				sizeof(csTemp),
				"%s:%hu",
				csAddress,
				lpauNode->usPort
			);

			if (ualIndex != lparRouter->ualEgressUnicastCount - 1)
			{ StringConcatenateSafe(csTemp, " ", sizeof(csTemp)); }

			StringConcatenateSafe(
				csBuffer,
				csTemp,
				sizeof(csBuffer) - 1
			);

		}

		StringSplit(
			csBuffer,
			" ",
			&dlpszSplit
		);
	}


	ReleaseSingleObject(lparRouter->hLock);

	return dlpszSplit;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetEgressUnicastStreams(
	_In_ 		HANDLE 		hAudioRouter
){
	EXIT_IF_UNLIKELY_NULL(hAudioRouter, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hAudioRouter), HANDLE_TYPE_AUDIO_ROUTER, FALSE);

	LPAUDIO_ROUTER lparRouter;
	DLPSTR dlpszSplit;
	UARCHLONG ualIndex;
	LPVOID lpTraversal;
	LPAUDIO_UNICAST lpauNode;
	CSTRING csBuffer[0x1000];
	CSTRING csTemp[0x1000];
	CSTRING csAddress[0x100];

	ZeroMemory(csBuffer, sizeof(csBuffer));
	ZeroMemory(csTemp, sizeof(csTemp));
	ZeroMemory(csAddress, sizeof(csAddress));

	lparRouter = (LPAUDIO_ROUTER)GetHandleDerivative(hAudioRouter);
	EXIT_IF_UNLIKELY_NULL(lparRouter, FALSE);
	dlpszSplit = NULLPTR;

	WaitForSingleObject(lparRouter->hLock, INFINITE_WAIT_TIME);

	lpauNode = NULLPTR;
	if (0 != lparRouter->ualIngressUnicastCount)
	{
		lpTraversal = LinkedListTraverse(lparRouter->hllIngressUnicast, NULLPTR);
		for (
			ualIndex = 0;
			ualIndex < lparRouter->ualIngressUnicastCount;
			ualIndex++, lpTraversal = LinkedListTraverse(lparRouter->hllIngressUnicast, lpTraversal)
		){
			if (NULLPTR != lpTraversal)
			{ lpauNode = (LPAUDIO_UNICAST)LinkedListAtNode(lparRouter->hllIngressUnicast, lpTraversal); }
			
			if (NULLPTR == lpauNode)
			{ continue; }
			
			ZeroMemory(csTemp, sizeof(csTemp));
			ZeroMemory(csAddress, sizeof(csAddress));

			ConvertIpv4AddressToString(
				lpauNode->ip4Address,
				csAddress,
				sizeof(csAddress)
			);

			StringPrintFormatSafe(
				csTemp,
				sizeof(csTemp),
				"%s:%hu",
				csAddress,
				lpauNode->usPort
			);

			if (ualIndex != lparRouter->ualIngressUnicastCount - 1)
			{ StringConcatenateSafe(csTemp, " ", sizeof(csTemp)); }

			StringConcatenateSafe(
				csBuffer,
				csTemp,
				sizeof(csBuffer) - 1
			);

		}

		StringSplit(
			csBuffer,
			" ",
			&dlpszSplit
		);
	}


	ReleaseSingleObject(lparRouter->hLock);

	return dlpszSplit;
}