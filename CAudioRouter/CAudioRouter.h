#ifndef GATEWAY_AUDIOROUTER_H
#define GATEWAY_AUDIOROUTER_H


#include "Defs.h"
#include "Prereqs.h"



typedef pa_sample_format_t 		SAMPLE_FORMAT;




_Success_(return != NULL_OBJECT, _Non_Locking_)
GATEWAY_API
HANDLE
CreateAudioRouter(
	VOID
);




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddIngressDevice(
	_In_ 		HANDLE 		hAudioRouter,
	_In_ 		LPCSTR RESTRICT	lpcszIngress,
	_In_		SAMPLE_FORMAT 	sfFormat,
	_In_ 		ULONG 		ulRate,
	_In_ 		BYTE		byChannels,
	_Out_Opt_ 	LPUARCHLONG 	lpualIdentifier
);




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterRemoveIngressDevice(
	_In_ 		HANDLE 		hAudioRouter,
	_In_ 		LPCSTR RESTRICT	lpcszIngress
);




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddEgressDevice(
	_In_ 		HANDLE 		hAudioRouter,
	_In_ 		LPCSTR RESTRICT	lpcszEgress,
	_In_ 		SAMPLE_FORMAT 	sfFormat,
	_In_ 		ULONG 		ulRate,
	_In_		BYTE 		byChannels,
	_Out_Opt_ 	LPUARCHLONG 	lpualIdentifier
);




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterRemoveEgressDevice(
	_In_ 		HANDLE 		hAudioRouter,
	_In_ 		LPCSTR RESTRICT	lpcszEgress
);




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddIngressMulticastRouting(
	_In_ 		HANDLE 		hAudioRouter,
	_In_Z_ 		LPCSTR 		lpcszGroupAddress,
	_In_Z_ 		LPCSTR 		lpcszBindAddress,
	_In_ 		USHORT 		usPort
);




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddEgressMulticastRouting(
	_In_ 		HANDLE 		hAudioRouter,
	_In_Z_ 		LPCSTR		lpcszGroupAddress,
	_In_ 		USHORT 		usPort
);




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddIngressUnicastDevice(
	_In_ 		HANDLE		hAudioRouter,
	_In_Z_ 		LPCSTR		lpcszUnicastAddress,
	_In_ 		USHORT 		usPort
);




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterAddEgressUnicastDevice(
	_In_ 		HANDLE		hAudioRouter,
	_In_Z_ 		LPCSTR		lpcszUnicastAddress,
	_In_ 		USHORT 		usPort
);




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterPause(
	_In_ 		HANDLE 		hAudioRouter
);




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterResume(
	_In_ 		HANDLE 		hAudioRouter
);




_Success_(return != FALSE, _Interlocked_Operation_)
GATEWAY_API
BOOL
AudioRouterGetStatus(
	_In_ 		HANDLE 		hAudioRouter,
	_Out_Z_		LPSTR 		lpszOutBuffer,
	_In_ 		UARCHLONG 	ualBufferSize
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetIngressDevices(
	_In_ 		HANDLE 		hAudioRouter
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetEgressDevices(
	_In_ 		HANDLE 		hAudioRouter
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetIngressMulticastStreams(
	_In_ 		HANDLE 		hAudioRouter
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetEgressMulticastStreams(
	_In_ 		HANDLE 		hAudioRouter
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetIngressUnicastStreams(
	_In_ 		HANDLE 		hAudioRouter
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
GATEWAY_API
DLPSTR
AudioRouterGetEgressUnicastStreams(
	_In_ 		HANDLE 		hAudioRouter
);




#endif