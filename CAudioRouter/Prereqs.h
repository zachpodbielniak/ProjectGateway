#ifndef GATEWAY_PREREQS_H
#define GATEWAY_PREREQS_H



#include <pulse/pulseaudio.h>
#include <pulse/simple.h>
#include <pulse/error.h>
#include <pulse/mainloop.h>
#include <pulse/mainloop-api.h>
#include <pulse/context.h>


#include <PodNet/PodNet.h>
#include <PodNet/Ascii.h>
#include <PodNet/CContainers/CContainers.h>
#include <PodNet/CNetworking/CIpv4.h>
#include <PodNet/CNetworking/CIpv6.h>
#include <PodNet/CNetworking/CSocket4.h>
#include <PodNet/CNetworking/CSocket6.h>
#include <PodNet/CSystem/CSystem.h>
#include <PodNet/CString/CString.h>


typedef pa_simple 		PULSE, *LPPULSE;
typedef pa_sample_spec		PULSE_SAMPLE, *LPPULSE_SAMPLE;



#endif