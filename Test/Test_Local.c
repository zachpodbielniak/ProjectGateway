#include "../CAudioRouter/CAudioRouter.h"






LONG Main(LONG lArgCounts, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCounts);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	HANDLE hRouter;

	hRouter = CreateAudioRouter();

	AudioRouterAddIngressDevice(
		hRouter,
		"alsa_output.platform-snd_aloop.0.analog-stereo.monitor",
		PA_SAMPLE_S16LE,
		44100,
		2,
		NULLPTR
	);

	AudioRouterAddEgressDevice(
		hRouter,
		"alsa_output.pci-0000_00_14.2.analog-surround-51",
		PA_SAMPLE_S16LE,
		44100,
		2,
		NULLPTR
	);

	AudioRouterAddEgressDevice(
		hRouter,
		"alsa_output.usb-Logitech_Logitech_G35_Headset-00.analog-stereo",
		PA_SAMPLE_S16LE,
		44100,
		2,
		NULLPTR
	);

	AudioRouterAddEgressMulticastRouting(hRouter, "239.255.175.222", 0xDEAF);

	WaitForSingleObject(hRouter, INFINITE_WAIT_TIME);
	return 0;	
}