#include "../CAudioRouter/CAudioRouter.h"


LONG Main(LONG lArgCounts, DLPSTR dlpszArgValues)
{
	UNREFERENCED_PARAMETER(lArgCounts);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	HANDLE hRouter;

	hRouter = CreateAudioRouter();

	AudioRouterAddEgressDevice(
		hRouter,
		"alsa_output.hw_0_0",
		PA_SAMPLE_S16LE,
		44100,
		2,
		NULLPTR
	);

	AudioRouterAddIngressMulticastRouting(
		hRouter,
		"239.255.175.222",
		"0.0.0.0",
		0xDEAF
	);


	WaitForSingleObject(hRouter, INFINITE_WAIT_TIME);
	return 0;
}


