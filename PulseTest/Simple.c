/* 
	To get this to work, be sure to:
		sudo modprobe snd_aloop
	and then redirect an audio output
	to "Built In Audio Stereo"

	To get all devices:
		pactl list | grep -A2 'Source #' | grep 'Name: ' | cut -d" " -f2
*/

#include <PodNet/PodNet.h>

#include <pulse/simple.h>
#include <pulse/error.h>
#include <mad.h>

#define BUFFER_SIZE 		0x4000


LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	LONG lRet, lError, lFd;
	pa_simple *s;
	pa_simple *s2;
	pa_simple *r;
	pa_sample_spec ss;

	ss.format = PA_SAMPLE_S16LE;
	ss.rate = 44100;
	ss.channels = 2;

	lFd = open(dlpszArgValues[1], O_RDONLY);

	dup2(lFd, STDIN_FILENO);

	close(lFd);
	
	r = pa_simple_new(
		NULL,
		dlpszArgValues[0],
		PA_STREAM_RECORD,
		"alsa_output.platform-snd_aloop.0.analog-stereo.monitor",
		"record",
		&ss,
		NULL,
		NULL,
		&lError
	);

	s = pa_simple_new(
		NULL,
		dlpszArgValues[0],
		PA_STREAM_PLAYBACK, 
		"alsa_output.pci-0000_00_14.2.analog-surround-51",
		"playback",
		&ss,
		NULL,
		NULL,
		&lError
	);

	s2 = pa_simple_new(
		NULL,
		dlpszArgValues[0],
		PA_STREAM_PLAYBACK,
		"alsa_output.usb-Logitech_Logitech_G35_Headset-00.analog-stereo",
		"playback",
		&ss,
		NULL,
		NULL,
		&lError
	);

	
	INFINITE_LOOP()
	{
		BYTE byBuffer[BUFFER_SIZE];
		ULONGLONG ullRead;

		/* Read the data */
		/*if ((ullRead = read(STDIN_FILENO, byBuffer, sizeof(byBuffer))) <= 0)
		{
			if (0 == ullRead) /* EOF */
			/*{ break; }

			fprintf(stderr, "read() failed: %s\n", strerror(lError));
			JUMP(__Finished__);
		}*/

		if (pa_simple_read(r, byBuffer, BUFFER_SIZE, &lError) < 0)
		{
			fprintf(stderr, "pa_simple_read() failed: %s\n", pa_strerror(lError));
			JUMP(__Finished__);
		}

		/* Write the data */
		if (pa_simple_write(s, byBuffer, BUFFER_SIZE, &lError) < 0)
		{
			fprintf(stderr, "s1: pa_simple_write() failed: %s\n", pa_strerror(lError));
			JUMP(__Finished__);
		}

		if (pa_simple_write(s2, byBuffer, BUFFER_SIZE, &lError) < 0)
		{
			fprintf(stderr, "s2: pa_simple_write() failed: %s\n", pa_strerror(lError));
			JUMP(__Finished__);
		}
	}

	/* Drain the player buffer */
	pa_simple_drain(s, &lError);
	pa_simple_drain(s2, &lError);
	lRet = 0;

	__Finished__:

	if (NULLPTR != s)
	{ 
		pa_simple_free(s); 
		pa_simple_free(s2);
	}

	return lRet;
}