#include <PodNet/PodNet.h>
#include <PodNet/CSystem/CSystem.h>

#include <pulse/pulseaudio.h>
#include <pulse/error.h>
#include <pulse/mainloop.h>
#include <pulse/mainloop-api.h>
#include <pulse/context.h>


#define BUFFER_SIZE 		0x0400

GLOBAL_VARIABLE HANDLE 	hPlayThread = NULL_OBJECT;



_Call_Back_
VOID
__ContextState(
	_In_ 		pa_context 		*pcCtx,
	_In_ 		LPVOID 			lpMainLoop
){ pa_threaded_mainloop_signal((pa_threaded_mainloop *)lpMainLoop, 0); }


_Call_Back_
VOID
__StreamState(
	_In_ 		pa_stream 		*psStream,
	_In_ 		LPVOID 			lpMainLoop
){ pa_threaded_mainloop_signal((pa_threaded_mainloop *)lpMainLoop, 0); }


_Call_Back_
VOID
__StreamSuccess(
	_In_ 		pa_stream 		*psStream,
	_In_ 		LONG 			lSuccess,
	_In_ 		LPVOID 			lpData
){ return; }


_Call_Back_
VOID
__StreamWrite(
	_In_ 		pa_stream 		*psStream,
	_In_ 		UARCHLONG 		ualRequestedBytes,
	_In_ 		LPVOID 			lpData
){
	UARCHLONG ualBytesRemaining;
	ULONGLONG ullBytesRead;
	LONG lFd;

	ualBytesRemaining = ualRequestedBytes;
	lFd = open("sound.raw", O_RDONLY);

	hPlayThread = GetCurrentThread();

	INFINITE_LOOP()
	{
		LPBYTE lpbyBuffer;
		ULONGLONG ullRead;

		pa_stream_begin_write(psStream, (DLPVOID)&lpbyBuffer, &ualRequestedBytes);

		ullRead = read(lFd, lpbyBuffer, ualRequestedBytes);
		if (0 == ullRead) /* EOF */
		{ break; }


		pa_stream_write(psStream, lpbyBuffer, ualRequestedBytes, NULLPTR, 0x00LL, PA_SEEK_RELATIVE);
	}

	
	/*
	while (0 < ualBytesRemaining)
	{
		LPBYTE lpbyBuffer;
		UARCHLONG ualBytesToFill;
		
		if (ualBytesToFill > ualBytesRemaining)
		{ ualBytesToFill = ualBytesRemaining; }

		pa_stream_begin_write(psStream, (DLPVOID)&lpbyBuffer, &ualBytesToFill);

		ullBytesRead = read(lFd, lpbyBuffer, ualBytesToFill);

		if (0 == ullBytesRead)
		{ break; }

		pa_stream_write(psStream, lpbyBuffer, ualBytesToFill, NULLPTR, 0LL, PA_SEEK_RELATIVE);
	}

/*
	while (0 < ualBytesRemaining)
	{
		LPBYTE lpbyBuffer;
		UARCHLONG ualBytesToFill;
		UARCHLONG ualIndex;

		ualBytesToFill = 44100;

		if (ualBytesToFill > ualBytesRemaining)
		{ ualBytesToFill = ualBytesRemaining; }


		pa_stream_begin_write(psStream, (DLPVOID)&lpbyBuffer, &ualBytesToFill);
		
		for (
			ualIndex = 0;
			ualIndex < ualBytesToFill;
			ualIndex += 2
		){
			lpbyBuffer[ualIndex] = (ualIndex % 100) * 40 / 100 + 44;
			lpbyBuffer[ualIndex + 1] = (ualIndex % 100) * 40 / 100 + 44;
		}
		
		pa_stream_write(psStream, lpbyBuffer, ualBytesToFill, NULLPTR, 0LL, PA_SEEK_RELATIVE);

		ualBytesRemaining -= ualBytesToFill;
	}
*/




}




LONG Main(LONG lArgCount, DLPSTR dlpszArgValues)
{
	pa_threaded_mainloop *ptmLoop;
	pa_mainloop_api *pmaApi;
	pa_context *pcCtx;
	pa_stream *psStream;
	pa_sample_spec pssSpecs;
	pa_channel_map pcmMap;
	pa_stream_flags_t psfFlags;
	pa_buffer_attr pbaAttrs;

	ptmLoop = pa_threaded_mainloop_new();
	if (NULLPTR == ptmLoop)
	{ DiePeacefully("Could not allocate main loop object.", NULLPTR); }	
	
	pmaApi = pa_threaded_mainloop_get_api(ptmLoop);
	if (NULLPTR == pmaApi)
	{ DiePeacefully("Could not get main loop api.", NULLPTR); }

	pcCtx = pa_context_new(pmaApi, "Project-Gateway");
	if (NULLPTR == pcCtx)
	{ DiePeacefully("Could not create new context.", NULLPTR); }

	pa_context_set_state_callback(pcCtx, __ContextState, ptmLoop);
	pa_threaded_mainloop_lock(ptmLoop);

	/* Start the main loop object */
	if (0x00 != pa_threaded_mainloop_start(ptmLoop))
	{ DiePeacefully("Could not start main loop object.", NULLPTR); }

	/* Connect context */
	if (0x00 != pa_context_connect(pcCtx, NULLPTR, PA_CONTEXT_NOAUTOSPAWN, NULLPTR))
	{ DiePeacefully("Could not connect context to server.", NULLPTR); }

	/* Wait for context to be ready */
	INFINITE_LOOP()
	{
		pa_context_state_t pacsState;
		pacsState = pa_context_get_state(pcCtx);

		if (PA_CONTEXT_READY == pacsState)
		{ break; }

		pa_threaded_mainloop_wait(ptmLoop);
	}

	/* Play back stream */
	pssSpecs.channels = 0x02;
	pssSpecs.rate = 44100;
	pssSpecs.format = PA_SAMPLE_S16LE;

	pa_channel_map_init_stereo(&pcmMap);

	psStream = pa_stream_new(pcCtx, "Playback", &pssSpecs, &pcmMap);

	pa_stream_set_state_callback(psStream, __StreamState, ptmLoop);
	pa_stream_set_write_callback(psStream, __StreamWrite, ptmLoop);

	pbaAttrs.maxlength = MAX_ULONG;
	pbaAttrs.tlength = MAX_ULONG;
	pbaAttrs.prebuf = MAX_ULONG;
	pbaAttrs.minreq = MAX_ULONG;

	psfFlags = PA_STREAM_START_CORKED;
	psfFlags |= PA_STREAM_INTERPOLATE_TIMING;
	psfFlags |= PA_STREAM_NOT_MONOTONIC;
	psfFlags |= PA_STREAM_AUTO_TIMING_UPDATE;
	psfFlags |= PA_STREAM_ADJUST_LATENCY;
	

	/* Connect audio to default audio output sink */
	if (0 != pa_stream_connect_playback(
		psStream,
		NULL,
		&pbaAttrs,
		psfFlags,
		NULL,
		NULL
	)){ DiePeacefully("Could not connect stream to default audio source.", NULLPTR); }
	
	

	/* Wait for stream to be ready */
	INFINITE_LOOP()
	{
		pa_stream_state_t pssState;
		pssState = pa_stream_get_state(psStream);
		if (PA_STREAM_READY == pssState)
		{ break; }
		pa_threaded_mainloop_wait(ptmLoop);
	}

	pa_threaded_mainloop_unlock(ptmLoop);

	pa_stream_cork(psStream, 0, __StreamSuccess, ptmLoop);

	while (NULL_OBJECT == hPlayThread) { }

	WaitForSingleObject(hPlayThread, INFINITE_WAIT_TIME);

	return 0;
}
